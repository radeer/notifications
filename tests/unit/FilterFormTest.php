<?php

use domain\forms\FilterForm;

class FilterFormTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testCorrectValidate()
    {
		$form = new FilterForm();
		$form->title = 'title';
		$form->description = 'description';
		
		$this->tester->assertTrue($form->validate());
    }
	
	public function testMissTitleValidate() {
		$form = new FilterForm();
		$form->description = 'description';
		
		$this->tester->assertFalse($form->validate());
	}
	
}