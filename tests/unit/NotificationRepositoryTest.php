<?php

use domain\repositories\NotificationRepository;
use domain\entities\Notification\Notification;
use domain\entities\Filter\Filter;
use domain\entities\Notification\NotificationFilter;
use domain\exceptions\CantSaveException;
use domain\exceptions\NotFoundException;

class NotificationRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
	protected $repository;
	protected $notification;

    protected function _before()
    {
		$this->repository = new NotificationRepository;
		$this->notification = new Notification(
			'notification title',
			[
				'heading' => 'notification heading',
				'content' => 'notification description'
			]
		);
		$this->notification->save();
		
    }

    protected function _after()
    {
		$this->notification->delete();
    }

    
    public function testGet()
    {
		$notification = $this->repository->get($this->notification->id);
		$this->assertInstanceOf(Notification::class, $notification);
    }
	
	public function testCantGet() {
		$id = -1;
		$this->tester->expectException(new NotFoundException('Не удалось найти рассылку по идентификатору ' . $id), function() use ($id){
			$this->repository->get($id);
		});
	}
	
	public function testSave() {
		$notification = new Notification(
			'new notification title',
			[
				'heading' => 'new notification heading',
				'content' => 'new notification description'
			]
		);
		$this->assertGreaterThan(0, $this->repository->save($notification));
		$notification->delete();
	}
	
	public function testCantSave() {
		$notification = new Notification(
			'new notification title',
			[
				'heading' => 'new notification heading',
				'content' => 'new notification description'
			]
		);
		$notification->title = null;
		
		$this->tester->expectException(new CantSaveException('Не удалось сохранить рассылку'), function() use ($notification){
			$this->repository->save($notification);
		});
	}
	
	public function testAddFilter() {
		$filter = new Filter('new test filter', 'new test filter description');
		$filter->save();
		$result_id = $this->repository->addFilter($this->notification, $filter->id);
		$this->assertGreaterThan(0, $result_id);
		
		$n_filter = NotificationFilter::findOne(['id' => $result_id]);
		$this->assertInstanceOf(NotificationFilter::class, $n_filter);
		$n_filter->delete();
		$filter->delete();
	}
	
	public function testCantAddFilter() {
		$this->tester->expectException(new CantSaveException('Не удалось добавить фильтр с идентификатором ' . null), function() {
			$result_id = $this->repository->addFilter($this->notification, null);
			if ($result_id > 0) {
				NotificationFilter::deleteAll(['id' => $result_id]);
			}
		});
	}
	
}