<?php

use domain\repositories\FilterRepository;
use domain\entities\Filter\Filter;
use domain\exceptions\NotFoundException;

class FilterRepositoryEnsureExistsTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
	protected $repository;
	protected $filter;

	protected function _before()
    {
		$this->repository = new FilterRepository;
		$filter = new Filter('test filter', 'test description');
		$filter->save();
		$this->filter = $filter;
    }

    protected function _after()
    {
		$this->filter->delete();
    }

    public function testCorrectWork() {
		$this->repository->ensureExists($this->filter->id);
		$this->repository->ensureExists([$this->filter->id]);
		
		$id = -1;
		$this->tester->expectException(new NotFoundException('Не удалось найти фильтр по идентификатору ' . $id), function() use ($id) {
			$this->repository->ensureExists($id);
		});
		$this->tester->expectException(new NotFoundException('Не удалось найти фильтр по идентификатору ' . $id), function() use ($id) {
			$this->repository->ensureExists([$id]);
		});
	}
	
	public function testEmptyParam() {
		$id = '';
		$this->tester->expectException(new NotFoundException('Не удалось найти фильтр по идентификатору ' . $id), function() use ($id) {
			$this->repository->ensureExists([$id]);
		});
		$this->tester->expectException(new NotFoundException('Не удалось найти фильтр по идентификатору ' . $id), function() use ($id) {
			$this->repository->ensureExists([$id]);
		});
	}
	
}