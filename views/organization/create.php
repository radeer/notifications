<?php

use yii\helpers\Html;


/**
 * @var $this yii\web\View
 * @var $model app\models\Organization
 * @var $filters \domain\entities\Filter\Filter
 */

$this->title = Yii::t('app', 'Create organization');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Organizations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organization-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'filters' => $filters,
    ]) ?>

</div>
