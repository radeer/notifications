<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrganizationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="organization-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'Match_Code') ?>

    <?= $form->field($model, 'mainaddress_adressname') ?>

    <?= $form->field($model, 'country') ?>

    <?= $form->field($model, 'zipcode') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'clerk_staffId') ?>

    <?php // echo $form->field($model, 'clerk_name') ?>

    <?php // echo $form->field($model, 'associationnumber') ?>

    <?php // echo $form->field($model, 'mainassociationnumber') ?>

    <?php // echo $form->field($model, 'agent_name') ?>

    <?php // echo $form->field($model, 'isoCode') ?>

    <?php // echo $form->field($model, 'top_customer') ?>

    <?php // echo $form->field($model, 'Accounting_clerk_staffId') ?>

    <?php // echo $form->field($model, 'Accounting_clerk_name') ?>

    <?php // echo $form->field($model, 'Common_Match_Code') ?>

    <?php // echo $form->field($model, 'mainaddress_addressname2') ?>

    <?php // echo $form->field($model, 'deliveryaddress_addressname') ?>

    <?php // echo $form->field($model, 'deliveryaddress_city') ?>

    <?php // echo $form->field($model, 'deliveryaddress_phone') ?>

    <?php // echo $form->field($model, 'deliveryaddress_fax') ?>

    <?php // echo $form->field($model, 'deliveryaddress_mobilephone') ?>

    <?php // echo $form->field($model, 'deliveryaddress_email') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
