<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model app\models\Organization */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="organization-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class='col-sm-6 col-md-6' id='id'>
        <?= $form->field($model, 'filters')->widget(Select2::class, [
            'options' => [
                'placeholder' => Yii::t('app', 'Select one or more filters'),
                'multiple' => true
            ],
            'data' => $filters,
            'pluginOptions' => [
                'allowClear' => true,
            ]
        ]) ?>

        <?= $form->field($model, 'mainaddress_adressname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Match_Code')->textInput(['maxlength' => true]) ?>



    <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zipcode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'clerk_staffId')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'clerk_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'associationnumber')->textInput() ?>

    <?= $form->field($model, 'mainassociationnumber')->textInput() ?>

    <?= $form->field($model, 'agent_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isoCode')->textInput(['maxlength' => true]) ?>

    <?=$form->field($model, 'top_customer')->dropDownList([
    '' => '',
    '0' => 'Да',
    '1' => 'Нет'
    ]); ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <div class='col-sm-6 col-md-6' id='id2'>
    <?= $form->field($model, 'Accounting_clerk_staffId')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Accounting_clerk_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Common_Match_Code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mainaddress_addressname2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deliveryaddress_addressname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deliveryaddress_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deliveryaddress_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deliveryaddress_fax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deliveryaddress_mobilephone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deliveryaddress_email')->textInput(['maxlength' => true]) ?>



    </div>


    <?php ActiveForm::end(); ?>



</div>

