<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use app\widgets\Alert;
use yii\helpers\Url;
use yii\widgets\ListView;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Organization */

$this->title = $model->mainaddress_adressname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contacts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>.select2-container--krajee.select2-container--disabled .select2-selection, .select2-container--krajee.select2-container--disabled .select2-selection--multiple .select2-selection__choice {background-color:white;}
    .select2-container--krajee.select2-container--disabled .select2-search__field, .select2-container--krajee.select2-container--disabled .select2-selection {cursor: pointer;}
</style>

<div class="organization-view">
    <?php
    $this->registerJs(
        '$("document").ready(function(){
            $("#new_note").on("pjax:end", function() {
            $.pjax.reload({container:"#notes"});  //Reload GridView
        });
    });'
    );
    ?>
    <?php $this->registerJs("
    $('#users tbody td').css('cursor', 'pointer');
    $('#users tbody td').on('click', function (e) {
        var id = $(this).closest('tr').data('key');
        if(e.target == this)
        location.href = '" . Url::to(['contacts/update']) . "?id=' + id;
        });
        ", yii\web\View::POS_END);


    ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="col-md-5 left-side-home-outer" style="padding-left: 0px;">
        <div class="card-box">
            <div class="table-responsive">
<?= Html::label(Yii::t('app', 'Directions')) ?>
            <?=Select2::widget([
    'name' => 'state_2',
   'readonly' => true,
    'hideSearch' => true,
    'showToggleAll' => false,
   'disabled' => true,
    'value' => $filters,
    //'theme' => 'classic',
    'options' => ['multiple' => true, 'placeholder' => Yii::t('app', 'Empty')],

]); ?>



    <?= DetailView::widget([
        'model' => $model,

        'attributes' => [
                'mainaddress_adressname',
        //    'id',
            'Match_Code',
          /* [
                'label' => 'Направления',
                'value' => $filters,
            ], */
                'country',
            'zipcode',
            'city',
            'clerk_staffId',
            'clerk_name',
            'associationnumber',
            'mainassociationnumber',
            'agent_name',
            'isoCode',
            'top_customer',
            'Accounting_clerk_staffId',
            'Accounting_clerk_name',
            'Common_Match_Code',
            'mainaddress_addressname2',
            'deliveryaddress_addressname',
            'deliveryaddress_city',
            'deliveryaddress_phone',
            'deliveryaddress_fax',
            'deliveryaddress_mobilephone',
            'deliveryaddress_email:email',
        ],
    ]) ?>
            </div></div></div>
    <div class="col-md-7 left-right-home-outer">
         <?php Pjax::begin(['enablePushState' => false,'timeout' => 5000,'id' => 'my_pjax']); ?>
        <div class="card-box">

            <div class="table-responsive">
                <?= GridView::widget([
        'dataProvider' => $dataProvider1,
        'summary' => '<h4>Контакты</h4>',
        //'filterModel' => $searchModel1,
        'emptyText' => Yii::t('app', 'There is no one contact'),
                       'tableOptions' => [ 'id' => 'users',
        'class' => 'table table-hover mails m-0 table table-actions-bar', ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

             'client_name',
           // 'addressname:ntext',

            'phone',
            'email:email',
            'birthday:date',
            'type',
           // [
           // 'label' => 'Организация',
           // 'value' => 'organization.mainaddress_adressname',
           // ],

        ],
    ]); ?>

            </div></div>
        <button class="btn btn-info" data-toggle="collapse" data-target="#hide-me"><?=Yii::t('app', 'Add contact')?></button>
        <div id="hide-me" class="collapse"><br>
            <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>

            <?= $form->field($contacts, 'client_name') ?>
            <?= $form->field($contacts, 'addressname') ?>
            <?= $form->field($contacts, 'phone') ?>
            <?= $form->field($contacts, 'email') ?>
            <?= $form->field($contacts, 'type') ?>
            <?php echo $form->field($contacts, 'birthday')->widget(\kartik\date\DatePicker::classname(), [
                'type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND,
                'pickerButton' => false,
                'language' => 'ru',
                'options' => [
                    'placeholder' => Yii::t('app', 'Select date')
                ],
                'pluginOptions' => [
                    'autoclose' => true,
                    'showMeridian' => true,
                    'format' => 'yyyy-mm-dd',
                    'minView' => 2,
                ]
            ]) ?>
            <?= Html::submitButton(Yii::t('app', 'Save contact'), ['class' => 'btn btn-primary']) ?>
            <br><br>

            <?php ActiveForm::end(); ?>
        </div><br> <br><?=Alert::widget()?><br>
        <div class="card-box">

            <div class="table-responsive">

         <?= GridView::widget([
        'dataProvider' => $dataProvider,
       'summary' => '<h4 style="text-align: center; margin: 0px;">Заметки</h4>',
           //  'filterModel' => $searchModel,
                'emptyText' => Yii::t('app', 'There is no notes'),
                 'tableOptions' => [
        'class' => 'table table-hover mails m-0 table table-actions-bar', ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			//'organization_id',

                [ 'attribute' => 'note',
              'format' => 'ntext' ],
           [ 'label' => 'Автор',
                'attribute' => 'user.username',
  ],

                'createdAt',
          ['class' => 'yii\grid\ActionColumn',
            'header' => 'Действия',
            'template' => '{delete}',
             'urlCreator' => function ($action, $model1, $key, $index) {
            if ($action === 'update') {
                return Url::to(['organization-notes/update', 'id' => $model1->id]);
              }
                else {
                return Url::to(['organization-notes/delete', 'id' => $model1->id]);
                }}


                ],
        ],
    ]); ?>
            </div>
        </div>
    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>
    <?= $form->field($model1, 'note')->label(Yii::t('app', 'New note')) ?>
    <?= Html::submitButton(Yii::t('app', 'Add note'), ['class' => 'btn btn-primary']) ?>
        <br><br>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>

    </div>
</div>
