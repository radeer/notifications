<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrganizationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contacts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organization-index">



    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add organization'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Upload from excel file'), ['excel'], ['class' => 'btn btn-primary']) ?>
    </p>

   <?php $this->registerJs("
    $('tbody td').css('cursor', 'pointer');
    $('tbody td').on('click', function (e) {
        var id = $(this).closest('tr').data('key');
        if(e.target == this)
        location.href = '" . Url::to(['organization/view']) . "?id=' + id;
        });
        ", yii\web\View::POS_END);


   ?>


    <?= Yii::$app->session->getFlash('success') ?>
<div class="card-box">
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
     'emptyText' => Yii::t('app', 'There is no one organization'),
        'tableOptions' => [
        'class' => 'table table-hover mails m-0 table table-actions-bar',
    ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'mainaddress_adressname',
           // 'country',
            //'zipcode',
    'agent_name',
        // 'clerk_name',
            'createdAt',
            //'city',
            //'clerk_staffId',

            //'associationnumber',
            //'mainassociationnumber',
            //'agent_name',
            //'isoCode',
            //'top_customer',
            //'Accounting_clerk_staffId',
            //'Accounting_clerk_name',
            //'Common_Match_Code',
            //'mainaddress_addressname2',
            //'deliveryaddress_addressname',
            //'deliveryaddress_city',
            //'deliveryaddress_phone',
            //'deliveryaddress_fax',
            //'deliveryaddress_mobilephone',
            //'deliveryaddress_email:email',


               ['class' => 'yii\grid\ActionColumn',
                'header' => 'Действия',
                'template' => '{update} {delete}'],
        ],
    ]); ?>
</div>
</div>
</div>
