<?php



use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TemplatesModel\TemplatesModel;


/* @var $this yii\web\View */
/* @var $model app\models\TemplatesModel\TemplatesModel */
/* @var $form ActiveForm */

$this->params['breadcrumbs'][] = [
    'template' => "<li><b>{link}</b></li>\n", //  шаблон для этой ссылки
    'label' => 'Notifications', // название ссылки
    'url' => ['/notifications'] // сама ссылка
];
$this->params['breadcrumbs'][] = ['label' => 'Templating'];
?>

<script type="text/javascript">
    midVal = "";
    prevVal = "";
    postval = "";
    var count = 0;
    setInterval(function(){

        var curVal = $("#contentt").val();

        if (prevVal !== curVal) {
            prevVal = curVal;
            //document.getElementById('preview').src="/web/template/frame?template="+postval;

            var iframe = document.getElementById('preview');

            iframe.contentWindow.document.getElementById('content').innerHTML = prevVal;



        }
    }, 100);

    function iframeLoaded() {
        var iFrameID = document.getElementById('preview');
        if(iFrameID) {
            iFrameID.height = 800 + "px";
            iFrameID.style = "display: block; position: absolute; top: 35px; left: 50%; border: 1px solid #cccc; transform: scale(0.8); width: 47%; border: none;";
        }
    }

    function getval(val) {

       // vall = $("#notificationform-id_template option:selected").text();
        maketemplate(val);
        // alert(vall);
    }

    function maketemplate(val) {
        var content = document.getElementById("contentt").value;

        if (val != "undefined") {

            if (val !== "") {
                postval = val;
                document.getElementById ('preview').src="/web/template/frame?template="+val; } }

    }
</script>



<iframe id="preview"  onload="iframeLoaded()" name="preview"  src=""></iframe>
<div style="width: 50%;">

    <?php $form = ActiveForm::begin(); ?>
	<?php 
		   $items = TemplatesModel::findtemplates('templates');
		   $params = [
        'prompt' => 'Выберите название шаблона',
        'onchange' => 'getval($(this).val());',
          ];
		  
	?>



    <?= \froala\froalaeditor\FroalaEditorWidget::widget([
    'model' => $model,
    'attribute' => 'content',
        'options' => [
            // html attributes
            'id'=>'contentt'
        ],
    'clientOptions' => [
        'width' => 600,
        'toolbarInline'=> false,
        'height' => 250,
        'theme' => 'royal',//optional: dark, red, gray, royal
        'language' => 'en_gb' ,
		'toolbarButtons' => ['html', 'bold', 'italic', '|','insertImage'],
        'imageUploadParam' => 'file',
        'imageUploadURL' => \yii\helpers\Url::to(['template/upload/'])
    ],
    'clientPlugins'=> ['code_view', 'image']
]); ?>

    <br>



    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
         Выбрать другой сохраненный шаблон
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
     <?= $form->field($model, 'template')->dropDownList($items,$params);?>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Использовать новый шаблон
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
          <?= \froala\froalaeditor\FroalaEditorWidget::widget([
    'model' => $model,
    'attribute' => 'text',
  'options' => [
        // html attributes

                ],
    'clientOptions' => [

        'toolbarInline'=> false,
        'height' => 250,
        'theme' => 'royal',//optional: dark, red, gray, royal
        'language' => 'en_gb' ,


		'toolbarButtons' => ['html', 'bold', 'italic', '|'],

    ],
    'clientPlugins'=> ['code_view']
]); ?>
 <?php echo $form->field($model, 'customtemplate');?>
      </div>
    </div>
  </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseTwo">
                           Тестовая отправка
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">

      <?php echo $form->field($model, 'test');?>
                 </div>
                    </div>
            </div>

		    <div class="form-group">
			<br>


            <br><?= Html::submitButton('Разослать', ['class' => 'btn btn-primary']) ?>


        </div>


    <?php ActiveForm::end(); ?>

</div>
   <!-- template -->
