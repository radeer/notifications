<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use conquer\codemirror\CodemirrorWidget;
use conquer\codemirror\CodemirrorAsset;
/* @var $this yii\web\View */
/* @var $model app\models\TemplatesModel\TemplatesModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="templates-model-form">



    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'template')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 't_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->widget(
        CodemirrorWidget::className(),
        [
            'assets'=>[
                CodemirrorAsset::MODE_CLIKE,
                CodemirrorAsset::KEYMAP_EMACS,
                   ],
            'settings'=>[
                'lineNumbers' => true,
                'mode' => 'text/x-csrc',
                'keyMap' => 'emacs',

            ],

        ]
    ); ?>

    <br>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
