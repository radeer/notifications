<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TemplatesModel\TemplatesModel */

$this->title = Yii::t('app', 'Update template');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->template, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="templates-model-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'content' => $content,
    ]) ?>

</div>
