<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TemplatesModel\SearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Templates');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="templates-model-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <p>
        <?= Html::a(Yii::t('app', 'Create template'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="card-box">
        <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
             'filterModel' => $searchModel,
                 'tableOptions' => [
        'class' => 'table table-hover mails m-0 table table-actions-bar', ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
			't_description',
            'template',



            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
    </div>
</div>