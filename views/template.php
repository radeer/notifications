<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TemplatesModel\TemplatesModel */
/* @var $form ActiveForm */
?>
<div class="template">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'template') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- template -->
