<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="wrapper-page">
    <div class=" card-box"><div class="panel-body">
            <h3 class="text-center"> Войти в <strong class="text-custom">KronoCRM</strong> </h3>

    <p></p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
       // 'layout' => 'horizontal',
       'options' => ['class' => 'form-horizontal m-t-20'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-xs-12\">{input}</div>\n<div class=\"col-xs-12\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-xs-12\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
        ]) ?>

        <div class="form-group">
            <div class="col-xs-12">
                <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-pink btn-block text-uppercase waves-effect waves-light', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
        </div></div></div>
</div>
