<<?php

use yii\bootstrap\Modal;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;

?>

<header id="topnav">
    <div class="topbar-main">
        <div class="container">

            <!-- Logo container-->
            <div class="logo">
                <a href="<?=\yii\helpers\Url::home()?>" class="logo"><span>Kron<i
                                class="md md-album"></i>CRM</span></a>
            </div>
            <!-- End Logo container-->


            <div class="menu-extras">


                <ul class="nav navbar-nav navbar-right pull-right">

                    <li class="dropdown navbar-c-items">
                        <a href="" class="dropdown-toggle waves-effect waves-light profile" data-toggle="dropdown"
                           aria-expanded="true">
                            <?php
                            echo Yii::$app->user->isGuest ? ('<a href="'.\yii\helpers\Url::to(['site/login']).'">'.Yii::t('app', 'Login').'</a>') : (Yii::$app->user->identity->username)
                            ?>


                        </a>
                        <ul class="dropdown-menu">
                            <?php if (!Yii::$app->user->isGuest) {
                                echo  '<li><a href="#">' .Html::beginForm(['/site/logout'], 'post') .'&nbsp;&nbsp;&nbsp;&nbsp;' .Html::submitButton(
                                        Yii::t('app', 'Logout'),
                                        ['class' => 'btn btn-link',
                                        ]
                                    )
                                   .Html::endForm()   . '</a></li>';


                            } ?>

                        </ul>
                    </li>
                </ul>
                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>

        </div>
    </div>

    <div class="navbar-custom">

            <div id="navigation">

<?php if (!Yii::$app->user->isGuest) {
                    NavBar::begin([
                        'options' => [
                            'class' => 'navigation-menu',

                        ],
                    ]);

                    echo   Nav::widget([
                        'options' => ['class' => 'navigation-menu'],
                        'items' => [
                            Yii::$app->user->can('admin') ?
                                ['label' => Yii::t('app', 'Users'), 'url' => ['/users/index'], 'options' => ['class' => 'has-submenu']]: '',
                           // ['label' => Yii::t('app', 'Index'), 'url' => ['/site/index'], 'options' => ['class' => 'has-submenu']],
                            ['label' => Yii::t('app', 'Filters'), 'url' => ['/filters/index'], 'options' => ['class' => 'has-submenu']],
                            ['label' => Yii::t('app', 'Contacts'), 'url' => ['/organization/index'], 'options' => ['class' => 'has-submenu']],

                            ['label' => Yii::t('app', 'Templates'), 'url' => ['/template/index'], 'options' => ['class' => 'has-submenu']],
                            ['label' => Yii::t('app', 'Notifications'), 'url' => ['/notifications/index'], 'options' => ['class' => 'has-submenu']],
                        ],
                    ]);
                    NavBar::end(); }
                else echo '<ul class="navigation-menu"><br><br><br></ul>';

                    ?>


        </div> <!-- end container -->
    </div> <!-- end navbar-custom -->
</header>

