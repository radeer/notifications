<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this); ?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript">document.cookie = "language=ru-RU; path=/; expires=" + date.toUTCString();</script>
        <link rel="shortcut icon" href="images/favicon_1.ico">

         <?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<?php $this->head() ?>

    </head>
    <body>
<?php $this->beginBody() ?>

        <?=$this->render('_sidebar');?>


        <div class="wrapper">
            <div class="container">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
      <?=$content;?>


                <?php
                $language = Yii::$app->request->cookies['language'];

                if ($language == "en-EN") {
                $url = "ru";
                $label = "RU";
                }
                else if ($language == "ru-RU") {
                    $url = "en";
                    $label = "EN";
                }
                else {
                    $url = "ru";
                    $label = "RU";
                }
                ?>

                <!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                Футер
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>

                                        <a href=<?=\yii\helpers\Url::to(['site/language', 'language' => $url])?>><?=$label?></a>

                                    </li>
                                    <li>

                                    </li>
                                    <li>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->

            </div>
        </div>

<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>