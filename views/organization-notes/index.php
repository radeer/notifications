<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrganizationNotesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Organization Notes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organization-notes-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Organization Notes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'note:ntext',
            'organization_id',
            'user_id',
            'createdAt',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
