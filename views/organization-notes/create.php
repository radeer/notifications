<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrganizationNotes */

$this->title = 'Create Organization Notes';
$this->params['breadcrumbs'][] = ['label' => 'Organization Notes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organization-notes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
