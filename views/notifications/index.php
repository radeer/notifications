<?php

use yii\helpers\Html;
use yii\grid\GridView;
use domain\widgets\NotificationAlert;

/* @var $this yii\web\View */
/* @var $searchModel domain\entities\Notification\NotificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Notifications');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="notification-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create notification'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	
	<?= NotificationAlert::widget(); ?>
    <div class="card-box">
        <div class="table-responsive">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
                       'tableOptions' => [
        'class' => 'table table-hover mails m-0 table table-actions-bar', ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
			],
			'title',
            'heading',
           [
			 'attribute' => 'content',
			 'format' => 'raw',
           ],
			'email:boolean',
            'sms:boolean',
			'template.t_description:ntext',
			[
				'format' => 'raw',
				'value' => function($model) {
			return Html::a(Yii::t('app', 'Send'), ['notifications/temp', 'id' => $model->id], ['class' => 'btn btn-info']);
				}
			],
            ['class' => 'yii\grid\ActionColumn',
            'header' => Yii::t('app', 'Actions'),
            'template' => '{update} {delete}'],
        ],
    ]); ?>
        </div>
    </div>

</div>
