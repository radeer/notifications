<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//use yii\bootstrap\Alert;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use app\widgets\Alert;


/* @var $this yii\web\View */
/* @var $model \domain\entities\Notification\NotificationForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $items \app\models\TemplatesModel\TemplatesModel */

?>

<?php if ($currenttemplate) {
    $script = <<< JS
     val = "$currenttemplate";
     text = '$model->content';
     document.getElementById('preview').src="../template/frame?template="+val;
      var iframe = document.getElementById('preview');
     
      function func() {
      iframe.contentWindow.document.getElementById('content').innerHTML = text;
      } 
     setTimeout(func, 1000); 
 
             
    
    
     //document.getElementById('html-1').setAttribute('aria-pressed', 'true');
JS;
    $this->registerJS($script, yii\web\View::POS_LOAD);
} ?>


<script type="text/javascript">
    midVal = "";
    prevVal = "";
    postval = "";
    var count = 0;


    setInterval(function () {
        var curVal = $("#contentt").val();
        if (prevVal !== curVal) {
            prevVal = curVal;
            var iframe = document.getElementById('preview');
            if (prevVal) {
                iframe.contentWindow.document.getElementById('content').innerHTML = prevVal;
            }
        }
    }, 100);

    function iframeLoaded() {
        var iFrameID = document.getElementById('preview');
        if (iFrameID) {
            iFrameID.height = 900 + "px";
            iFrameID.style = "width: 800px;display: block; position: relative; top: -100px; transform: scale(0.9); width: 100%; border: none;";
        }
    }

    function getval() {
        vall = $("#notificationform-id_template option:selected").val();
        maketemplate(vall);
    }

    function maketemplate(val) {
        if (val != "undefined") {
            if (val !== "") {
                postval = val;
                document.getElementById('preview').src = "../template/frame?template=" + val;
                document.getElementById('preview').hidden = false;
            }
        }

    }
</script>





<div class="col-md-6">
    <?php Pjax::begin() ?>
    <?=Alert::widget()?>
    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>

     <?= $form->field($model, 'filters')->widget(Select2::class, [
        'options' =>
            [
                'placeholder' => 'Выберите один или несколько фильтров',
                'multiple' => true
            ],
        'data' => $filters,
        'pluginOptions' =>
            [
                'allowClear' => true,
            ]
    ]) ?>

    <?= $form->field($model, 'title')->textInput(); ?>

    <?= $form->field($model, 'heading')->textInput(); ?>

    <?php
    $params = [
        'prompt' => 'Выбор шаблона',
        'onchange' => 'getval();',
        'options' =>
            [
                $currenttemplate => ['Selected' => true],
            ]
    ];
    ?>



    <?= $form->field($model, 'id_template')->dropDownList($templates, $params); ?>

    <?= \froala\froalaeditor\FroalaEditorWidget::widget([
        'model' => $model,
        'attribute' => 'content',
        'options' =>
            [
                // html attributes
                'id' => 'contentt',
            ],
        'clientOptions' =>
            [
                'toolbarInline' => false,
                'height' => 200,
                'theme' => 'royal',//optional: dark, red, gray, royal
                'language' => 'en_gb',
                'placeholderText' => 'Контент сообщения',
                'toolbarButtons' => ['html', 'bold', 'italic', '|', 'insertImage'],
                'imageUploadParam' => 'file',
                'imageUploadURL' => \yii\helpers\Url::to(['template/upload/'])
            ],
        'clientPlugins' => ['code_view', 'image']
    ]); ?>

    <?= $form->field($model, 'email')->checkbox() ?>

    <?= $form->field($model, 'sms')->checkbox() ?>




        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success', 'id' => 'subbut']) ?>

        <?php if ($model->id>0) echo Html::a(Yii::t('app', 'Send'), ['notifications/temp', 'id' => $model->id], ['class' => 'btn btn-success']) ?>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end() ?>
</div>

<div class="col-md-6">
    <iframe id="preview" onload="iframeLoaded()" name="preview" src="" hidden></iframe>
</div>
