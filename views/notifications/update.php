<?php

use yii\helpers\Html;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model domain\entities\Notification\Notification */

$this->title = Yii::t('app', 'Update notification') . ' '. $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Notifications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="notification-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'filters' => $filters,
        'templates' => $templates,
        'currenttemplate' => $currenttemplate,
    ]) ?>

</div>
