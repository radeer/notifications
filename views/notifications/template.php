<?php



use yii\helpers\Html;
use yii\widgets\ActiveForm;
use domain\widgets\NotificationAlert;
use yii\helpers\Url;
use yii\bootstrap\Tabs;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model domain\entities\Notification\NotificationForm */
/* @var $form ActiveForm */

$this->params['breadcrumbs'][] = [
   // 'template' => "<li><b>{link}</b></li>\n", //  шаблон для этой ссылки
    'label' => Yii::t('app', 'Notifications'), // название ссылки
    'url' => ['/notifications'] // сама ссылка
];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dispatch')];
?>

<?php
$content = $model->content;
$script = <<< JS
var content = '$content';
JS;

$this->registerJs($script, yii\web\View::POS_END); ?>


<script type="text/javascript">


    window.onload = function() {
            var iFrameID = document.getElementById('preview');
            iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + 'px';
            iFrameID.contentWindow.document.getElementById('content').innerHTML = content;

        }

</script>

<?php $form = ActiveForm::begin(); ?>
<div>
    <?= NotificationAlert::widget(); ?>
    <iframe id="preview" style="display: block; border: 1px solid #ccc; width: 65%; margin: auto; " src="<?= Url::to(['template/frame?template='])?><?=$template?>"></iframe>
    <br>

    <?php $test = $form->field($model, 'test')->textinput(['value' => $test]) ?>
    <br><?php $test .= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-info', 'formaction' => 'temp?id='.$model->id]) ?>

         <?php $tab = 'Количество получателей: ' .$count .' <br><br>'; $tab .= Html::a(Yii::t('app', 'Send all'), ['notifications/send', 'id' => $model->id],['data' => ['confirm' => 'Отправить рассылку '.$count.' контакту(ам)?'], 'class' => 'btn btn-success']);?>

    <?=Html::a(Yii::t('app', 'Edit'), ['notifications/update', 'id' => $model->id], ['class' => 'btn btn-primary']);?>
  <br><br>
        </div>
<?php $one =  $form->field($model, 'contact')->widget(Select2::classname(), [
    'data' => $users,
    'language' => 'ru',
    'options' => ['placeholder' => 'Выберите получателя'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]);

$one .=   Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-info', 'formaction' => 'sendone?id='.$model->id]); ?>

<?= Tabs::widget([
    'items' => [
        [
            'label' => Yii::t('app', 'Send for test'),
            'content' => $test,
        ],
        [
            'label' => Yii::t('app', 'Send to all segments'),
            'content' => $tab,
           // 'active' => true // указывает на активность вкладки
        ],
        [
            'label' => Yii::t('app', 'Send to one contact'),
            'content' => $one,
        ],


    ]
]); ?>


<?php ActiveForm::end(); ?>

</div>

