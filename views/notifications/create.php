<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model domain\entities\Notification\Notification */
/* @var $filters \domain\forms\FilterForm */

$this->title = Yii::t('app', 'Create notification');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Notifications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-create">


    <h1><?= Html::encode($this->title) ?></h1>

    

    <?= $this->render('_form', [
        'model' => $model,
		'filters' => $filters,
        'templates' => $templates,
        'currenttemplate' => null,
    ]) ?>

</div>
