<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model domain\entities\Filter\Filter */

$this->title = Yii::t('app', 'Create filter');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Filters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filter-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
