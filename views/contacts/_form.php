<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model domain\entities\Contact\ContactFrom */
/* @var $form yii\widgets\ActiveForm */

$error = null;
if (Yii::$app->session->hasFlash('domainError')) {
	$error = Yii::$app->session->getFlash('domainError');
}

?>


<div class="contact-form">

	<?php
		if ($error !== null) {
			echo Alert::widget([
				'options' => [
					'class' => 'alert-danger'
				],
				'body' => $error->getMessage()
			]);
		}
	?>

    <?php
    $params =   [
        'prompt' => Yii::t('app', 'Select organization'),
        'options' =>
            [
                $currentorg => ['Selected' => true],
            ]
            ];
    ?>
	
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'addressname')->textInput() ?>

    <?= $form->field($model, 'client_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'organization_id')->dropDownList($organizations,$params);?>

    <?php echo $form->field($model, 'birthday')->widget(\kartik\date\DatePicker::classname(), [
        'type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND,
        'pickerButton' => false,
        'language' => 'ru',
        'options' => [
            'placeholder' => Yii::t('app', 'Select date'),
        ],
        'pluginOptions' => [
            'autoclose' => true,
            'showMeridian' => true,
            'format' => 'yyyy-mm-dd',
            'minView' => 2,
        ]
    ]) ?>



	
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
