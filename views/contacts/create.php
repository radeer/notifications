<?php

use yii\helpers\Html;


/**
 * @var $this yii\web\View
 * @var $organizations \app\models\Organization
 * @var $model domain\entities\Contact\Contact
 **/

$this->title = Yii::t('app', 'Create contact');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contacts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'organizations' => $organizations,
        'currentorg' => null,
    ]) ?>

</div>
