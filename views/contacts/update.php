<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model domain\entities\Contact\Contact */

$this->title = Yii::t('app', 'Update contact'). ' ' .$model->client_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contacts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->client_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="contact-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'currentorg' => $currentorg,
        'organizations' => $organizations,
    ]) ?>

</div>
