<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel domain\entities\Contact\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contacts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create contact'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="card-box">
        <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
                       'tableOptions' => [
        'class' => 'table table-hover mails m-0 table table-actions-bar', ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'addressname:ntext',
            'client_name',
            'phone',
            'email:email',
            'birthday:date',

            [
            'label' => Yii::t('app', 'Organization'),
            'value' => 'organization.mainaddress_adressname',
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
    </div>
</div>
