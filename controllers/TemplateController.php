<?php

namespace app\controllers;

use Yii;
use domain\services\TemplateService;
//use domain\forms\Template\TemplateForm;
use app\models\TemplatesModel\TemplatesModel;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\TemplatesModel\SearchModel;
use app\models\UploadForm;
use yii\web\UploadedFile;
use yii\web\Response;

class TemplateController extends \yii\web\Controller
{
    protected $templateService;
    public  $file;

    public function __construct(
        $id,
        $module,
        TemplateService $templateService,
        $config = array()
    )
    {
        parent::__construct($id, $module, $config);
        $this->templateService = $templateService;


    }


    public function behaviors()
    {

        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TemplatesModel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TemplatesModel model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TemplatesModel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TemplatesModel();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->templateService->dobuild($model->text, $model->template);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TemplatesModel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $namefile = implode(TemplatesModel::gethtml($id));
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if (file_exists("../mail/layouts/" . $namefile . ".php")) {
                $file = fopen("../mail/layouts/" . $namefile . ".php", "w");
                fwrite($file, $model->text);
                fclose($file);
            }
                return $this->redirect(['view', 'id' => $model->id]);
            }


            if ($namefile != "") {
                $content = file_get_contents("../mail/layouts/" . $namefile . ".php");
                $model->text = $content;
            }
            return $this->render('update', [
                'model' => $model,
                'content' => $content,
            ]);
        }



    /**
     * Deletes an existing TemplatesModel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

	


    public function actions()
    {
        $this->enableCsrfValidation = false;
        return [
            '1image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => 'http://qwe/uploads', // Directory URL address, where files are stored.
                'path' => '../uploads', // Or absolute path to directory where files are stored.
            ],
        ];
    }


    public function actionUpload() {

        $base_path = Yii::getAlias('@app');
        $web_path = 'http://92.63.105.132';
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstanceByName('file');

            if ($model->validate()) {
                $model->file->saveAs($base_path . '/web/uploads/' . $model->file->baseName . '.' . $model->file->extension);
            }
        }


        $res = [
            'link' => ($web_path . '/uploads/' . $model->file->baseName . '.' . $model->file->extension),
        ];

        // Response data
        Yii::$app->response->format = Yii::$app->response->format = Response::FORMAT_JSON;
        return $res;
    }



    public function actionFrame ($template)
    { //   $form = new TemplatesModel();
        if ($template!=="")
          //  if ($form->load(Yii::$app->request->post())) {
                return $this->renderPartial('@app/mail/layouts/' . $template, ['content' => null]);
            //}


    }



    public function actionTemp($id) {
        $form = new TemplatesModel();
       // $preview = false;
        if ($form->load(Yii::$app->request->post()))
        {
            $this->templateService->rebuild($form);
            return $this->redirect(['notifications/send?id='.$id]);

        }

        return $this->render('template', [
            'model' => $form,]);



    }

    protected function findModel($id)
    {
        if (($model = TemplatesModel::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Шаблон с id='.$id.' не найден');
    }

    public function beforeAction($action) {

        if (Yii::$app->user->isGuest)
            $this->redirect('../site/login');

        return parent::beforeAction($action);
    }

}
