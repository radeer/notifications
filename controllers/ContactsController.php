<?php

namespace app\controllers;

use app\models\Organization;
use Yii;
use yii\helpers\ArrayHelper;
use domain\entities\Contact\Contact;
use domain\entities\Contact\ContactSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use domain\services\ContactService;
use domain\services\FilterService;
use domain\forms\Contact\ContactForm;

/**
 * ContactsController implements the CRUD actions for Contact model.
 */
class ContactsController extends Controller
{
	
	private $contactService;
	private $organization;

	public function __construct($id, $module, ContactService $contactService, Organization $organization, $config = array()) {
		parent::__construct($id, $module, $config);
		$this->contactService = $contactService;
		$this->organization = $organization;

	}
	
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contact models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContactSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contact model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
		$form = $this->contactService->findById($id);
        return $this->render('view', [
            'model' => $form,
        ]);
    }

    /**
     * Creates a new Contact model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new ContactForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
			try {
				$id = $this->contactService->create($form);
				return $this->redirect(['view', 'id' => $id]);
			} catch (\Exception $e) {
				Yii::$app->session->setFlash('domainError', $e);
			}
        }
        $array = Organization::find()->all();
        $organizations = ArrayHelper::map($array, 'id', 'mainaddress_adressname');
        return $this->render('create', [
            'model' => $form,
            'organizations' => $organizations,
        ]);
    }

    /**
     * Updates an existing Contact model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $form = $this->contactService->findById($id);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
			try {
				$this->contactService->edit($form);
				return $this->redirect(['organization/view', 'id' => $form->organization_id]);
			} catch (\Exception $e) {
				Yii::$app->session->setFlash('domainError', $e);
			}
        }
        $organizations = $this->contactService->getallorg();
        $currentorg = $this->contactService->getcurrentorg($form->organization_id);
        return $this->render('update', [
            'model' => $form,
            'currentorg' => $currentorg,
            'organizations' => $organizations,
        ]);
    }

    /**
     * Deletes an existing Contact model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {    if (Yii::$app->user->can('admin')) {
		$this->contactService->deleteById($id); }
		else Yii::$app->session->setFlash('error', 'У вас недостаточно прав для данной операции');
        return $this->redirect(['index']);
    }

    public function beforeAction($action) {

        if (Yii::$app->user->isGuest)
            $this->redirect('../site/login');

        return parent::beforeAction($action);
    }
}
