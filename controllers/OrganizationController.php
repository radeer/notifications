<?php

namespace app\controllers;

use app\models\ContactForm;
use app\models\OrganizationFilter;
use app\models\OrganizationNotes;
use app\models\OrganizationNotesSearch;
use app\models\User;
use domain\entities\Contact\Contact;
use domain\entities\Contact\ContactSearch;
use domain\entities\Filter\Filter;
use domain\exceptions\CantSaveException;
use Yii;
use app\models\Organization;
use app\models\OrganizationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UploadForm;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use domain\services;

/**
 * OrganizationController implements the CRUD actions for Organization model.
 */
class OrganizationController extends Controller
{
    public $repository;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        //Yii::$app->language = 'en-EN';
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Organization models.
     * @return mixed
     */


    public function actionIndex()
    {

        $searchModel = new OrganizationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Organization model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {   $model1 = new OrganizationNotes(['organization_id' => $id, 'user_id' => Yii::$app->user->id]);
        $searchModel = new OrganizationNotesSearch(['organization_id' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];
        $dataProvider->pagination->pageSizeLimit = ['1', '10'];

        $searchModel1 = new ContactSearch(['organization_id' => $id]);
        $dataProvider1 = $searchModel1->search(Yii::$app->request->queryParams);

        $query = OrganizationFilter::find()->select('filter_id')->where(['organization_id' => $id])->all();
        $query = ArrayHelper::getColumn($query, 'filter_id');
        $filters = [];
        foreach ($query as $key => $filter) {
           $q =  ArrayHelper::getColumn(Filter::find()->where(['id' => $filter])->all(), 'description');
           array_push($filters, implode($q));
        }
      //  $client_name = null;
        $contacts = new Contact('','',$id,'', '', '', '');
        if ($contacts->load(Yii::$app->request->post()) && $contacts->save()) {
          //  $contacts = new Contact('', '', $id);
            Yii::$app->session->setFlash('success', "Контакт сохранен");
            if (!Yii::$app->request->isPjax) {
                return $this->redirect(['organization/view'], ['id' => $id]);

            }
        }

        if ($model1->load(Yii::$app->request->post()) && $model1->save()) {
            $model1->note = "";
            Yii::$app->session->setFlash('success', "Заметка сохранена");
            if (!Yii::$app->request->isPjax) {
               return $this->redirect(['organization/view'], ['id' => $id]);
            }
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'model1' => $model1,
            'contacts' => $contacts,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModel1' => $searchModel1,
            'dataProvider1' => $dataProvider1,
            'filters' => $filters,
        ]);

    }

    /**
     * Creates a new Organization model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Organization();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        $filters = ArrayHelper::map(Filter::find()->all(), 'id', 'title');
        return $this->render('create', [
            'model' => $model,
            'filters' => $filters,
        ]);
    }

    /**
     * Updates an existing Organization model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
       /* $note = new OrganizationNotes;
        $note->organization_id = $id;
        $note->user_id = Yii::$app->user->id; */
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        $filters = ArrayHelper::map(Filter::find()->all(), 'id', 'title');
        return $this->render('update', [
            'model' => $model,
            'filters' => $filters,
        //    'note' => $note,
        ]);
    }

    /**
     * Deletes an existing Organization model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        if (Yii::$app->user->can('admin')) {
            try {
                $this->findModel($id)->delete();
            } catch (\Exception $exception) {
                Yii::$app->session->setFlash('error', 'У организации есть контакты или заметки. Удалить невозможно');
            }
        }
        else Yii::$app->session->setFlash('error', 'У вас недостаточно прав для данной операции');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Organization model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Organization the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Organization::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @return string|\yii\web\Response
     * @throws \PHPExcel_Reader_Exception
     */
    public function actionExcel()
    {
        if (Yii::$app->user->can('admin')) {
            $model = new Organization();
           // $contact = new Contact('', '', '', '', '', '');
            if (Yii::$app->request->isPost) {
                $model->excelfile = UploadedFile::getInstance($model, 'excelfile');
                if ($model->upload()) {
                    $sure = false;
                    $count = 0;
                    $unic = 0;
                    $input = array_keys($model->attributes);
                    array_shift($input);
                    array_pop($input);
                    $base_path = Yii::getAlias('@app');
                    $excel = \PHPExcel_IOFactory::load($base_path . '/excelupload/' . $model->excelfile->name);
                    foreach ($excel->getWorksheetIterator() as $worksheet) {
                        $lists[] = $worksheet->toArray();
                    }
                    foreach ($lists as $list) {
                        foreach ($list as $row) {
                            if ($sure == false) {
                                $sure = true;
                            } else {
                                if (count($row) > 20) {
                                    array_pop($row);
                                    $masiv = array_combine($input, $row);
                                    $model->attributes = $masiv;

                                    try {

                                        $model->save(false);
                                        $unic++;
                                        $contact = new Contact('', $model->agent_name, $model->id, '', '', '', 'agent');
                                        $contact->save(false);
                                        $contact = new Contact('', $model->clerk_name, $model->id, '', '', '', 'clerk');
                                        $contact->save(false);
                                    } catch (\Exception $e) {
                                    }
                                    $model->id = FALSE;
                                    $model->isNewRecord = TRUE;
                                    $count++;
                                }
                            }
                            foreach ($row as $col) {
                            }
                        }
                    }
                    Yii::$app->session->setFlash('success', $count . ' организаций было обнаружено. Из них ' . $unic . ' уникальная(ых) запись(ей) было добавлено.');
                    return $this->redirect('index');
                }
            }
        }
        else {Yii::$app->session->setFlash('error', 'У вас недостаточно прав для данной операции');  return $this->redirect(Yii::$app->request->referrer);}
        return $this->render('excel', ['model' => $model]);
    }

}