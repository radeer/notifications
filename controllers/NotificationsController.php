<?php

namespace app\controllers;

use app\models\TemplatesModel\TemplatesModel;
use domain\entities\Contact\Contact;
use Yii;
use domain\entities\Notification\NotificationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use domain\services\NotificationService;
use domain\services\FilterService;
use domain\forms\Notification\NotificationForm;
use domain\services\NotificationDispatcher;
use app\models\User;
use domain\entities\Notification\NotificationFilter;
use app\models\OrganizationFilter;


/**
 * NotificationsController implements the CRUD actions for Notification model.
 */
class NotificationsController extends Controller
{

    protected $notificationService;
    protected $filterService;
    protected $notificationDispatcher;
    protected $templatesModel;

    public function __construct(
        $id,
        $module,
        NotificationService $notificationService,
        TemplatesModel $templatesModel,
        FilterService $filterService,
        NotificationDispatcher $notificationDispatcher,
        $config = array()
    )
    {
        parent::__construct($id, $module, $config);
        $this->notificationService = $notificationService;
        $this->filterService = $filterService;
        $this->notificationDispatcher = $notificationDispatcher;
        $this->templatesModel = $templatesModel;

    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Notification models.
     * @return mixed
     */
    public function actionIndex()
    {


        $searchModel = new NotificationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Notification model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $form = $this->notificationService->findById($id);
        return $this->render('view', [
            'model' => $form,
        ]);
    }

    /**
     * Creates a new Notification model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $form = new NotificationForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            if (Yii::$app->request->isPjax) {
                try {
                    $id = $this->notificationService->create($form);
                    Yii::$app->session->setFlash('success', 'Рассылка сохранена');
                    $form->id = $id;
//	return $this->redirect(['view', 'id' => $id]);
                } catch (\Exception $e) {
                    Yii::$app->session->setFlash('domainError', $e);
                }
            }
        }
        $filters = ArrayHelper::map($this->filterService->all(), 'id', 'title');
        $templates = $this->templatesModel->findtemplates('notification');
        return $this->render('create', [
            'templates' => $templates,
            'model' => $form,
            'filters' => $filters,

        ]);
    }

    /**
     * Updates an existing Notification model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $form = $this->notificationService->findById($id);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $id = $this->notificationService->edit($form);
                return $this->redirect(['view', 'id' => $id]);
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('domainError', $e);
            }
        }

        $filters = ArrayHelper::map($this->filterService->all(), 'id', 'title');
        $templates = $this->templatesModel->findtemplates('notification');
        $currenttemplate = $this->templatesModel->findcurrenttemp($id);
        return $this->render('update', [
            'model' => $form,
            'filters' => $filters,
            'templates' => $templates,
            'currenttemplate' => $currenttemplate,
        ]);
    }

    /**
     * Deletes an existing Notification model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return \redirect to index
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->notificationService->deleteById($id);

        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     */
    public function actionSend($id)
    {
        try {
            $this->notificationDispatcher->notifyById($id);
            Yii::$app->session->setFlash('successNotification', true);
        } catch (\Exception $e) {
            Yii::$app->session->setFlash('domainError', $e);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionSendone($id) {
        $form = new NotificationForm();
        if ($form->load(Yii::$app->request->post())) {
            if ($form->contact !== "") {
                $this->notificationDispatcher->notifyByContactId($form->contact, $id);
                Yii::$app->session->setFlash('success', 'Рассылка успешно отправлена');
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionTemp($id)
    {   $filters = ArrayHelper::map($this->filterService->all(), 'id', 'title');
        $a = NotificationFilter::find()->where(['notification_id' => $id])->all();
        $b = ArrayHelper::getColumn($a,'filter_id');
        $c = OrganizationFilter::find()->where(['filter_id' => $b])->all();
        $e = ArrayHelper::getColumn($c,'organization_id');
        $r = Contact::find()->where(['organization_id' => $e])->all();
        $count = count($r);
        $array = Contact::find()->all();
        $users = ArrayHelper::map($array, 'email', 'client_name');
        $session = Yii::$app->session;
        $session->open();
        $form = $this->notificationService->findById($id);
        if ($form->load(Yii::$app->request->post())) {
            if ($form->test !== "") {
                $this->notificationDispatcher->notifyTest($form->test, $id);
                Yii::$app->session->setFlash('success', 'Тестовая рассылка успешно отправлена');
            }
        }
        $items = TemplatesModel::findonetemp($form->id_template);
        $template = implode($items);
        $session['template'] = $template;
        $model = User::find()->where(['username' => Yii::$app->user->identity->username])->one();
        return $this->render('template', [
            'model' => $form,
            'users' => $users,
            'template' => $template,
            'test' => $model->email,
            'count' => $count,
            'filters' => $filters]);
    }


}
