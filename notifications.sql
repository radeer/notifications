-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 06 2018 г., 21:45
-- Версия сервера: 5.6.38
-- Версия PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `notifications`
--

-- --------------------------------------------------------

--
-- Структура таблицы `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `addressname` text NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `sigment1` text,
  `sigment2` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `contacts`
--

INSERT INTO `contacts` (`id`, `addressname`, `client_name`, `phone`, `email`, `sigment1`, `sigment2`) VALUES
(3, 'Это я', 'Dmitriy', '89173588906', 'dmitrimaslyako@yandex.ru', 'qwe', 'qwe');

-- --------------------------------------------------------

--
-- Структура таблицы `contacts_filters`
--

CREATE TABLE `contacts_filters` (
  `id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `contacts_filters`
--

INSERT INTO `contacts_filters` (`id`, `contact_id`, `filter_id`) VALUES
(18, 3, 1),
(19, 3, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `filters`
--

CREATE TABLE `filters` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL COMMENT 'Название фильтра',
  `description` text COMMENT 'Описание'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `filters`
--

INSERT INTO `filters` (`id`, `title`, `description`) VALUES
(1, 'filter 1', 'filter 1 description'),
(2, 'filter 2', 'filter 2 description');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1516619894),
('m180118_120015_create_filters_table', 1516619896),
('m180118_121043_create_notifications_table', 1516619896),
('m180118_121813_create_notifications_filters_table', 1516619896),
('m180118_122557_create_contacts_table', 1516619896),
('m180118_123128_create_contacts_filters_table', 1516619896);

-- --------------------------------------------------------

--
-- Структура таблицы `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL COMMENT 'Название рассылки',
  `heading` varchar(255) DEFAULT NULL COMMENT 'Заголовок сообщения',
  `content` text COMMENT 'Тело сообщения',
  `email` tinyint(1) DEFAULT '1',
  `sms` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `notifications`
--

INSERT INTO `notifications` (`id`, `title`, `heading`, `content`, `email`, `sms`) VALUES
(1, 'notification 1', 'heading', '<p><u><em><strong>notification content</strong></em></u></p>\r\n', 0, 0),
(2, 'Тема', 'Заголовок', 'Привет, это тест', 1, 0),
(3, 'Тема уведомления', 'Заголовок новости!', 'Добрый день, это тестовая новость!', 1, 0),
(4, 'Title 2', 'Privet!', 'Tesoviy content', 1, 0),
(5, 'Это я просто пытаюсь', 'Хэдинг', '<p><u><em><strong>Контент, который мы передаём</strong></em></u></p>\r\n', 1, 0),
(6, '312', '321', '13', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `notifications_filters`
--

CREATE TABLE `notifications_filters` (
  `id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `notifications_filters`
--

INSERT INTO `notifications_filters` (`id`, `notification_id`, `filter_id`) VALUES
(12, 4, 1),
(13, 2, 1),
(22, 3, 1),
(23, 6, 1),
(24, 6, 2),
(25, 1, 1),
(26, 5, 1),
(27, 5, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `templates`
--

CREATE TABLE `templates` (
  `id` int(11) NOT NULL,
  `template` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `templates`
--

INSERT INTO `templates` (`id`, `template`) VALUES
(1, 'l_html'),
(2, 'l_html_2');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contacts_filters`
--
ALTER TABLE `contacts_filters`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_contact_id_filter_id` (`contact_id`,`filter_id`),
  ADD KEY `fk_contacts_filters_filters` (`filter_id`);

--
-- Индексы таблицы `filters`
--
ALTER TABLE `filters`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `notifications_filters`
--
ALTER TABLE `notifications_filters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_notifs_filters_notifications` (`notification_id`),
  ADD KEY `fk_notifs_filters_filters` (`filter_id`);

--
-- Индексы таблицы `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `template` (`template`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `contacts_filters`
--
ALTER TABLE `contacts_filters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `filters`
--
ALTER TABLE `filters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `notifications_filters`
--
ALTER TABLE `notifications_filters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT для таблицы `templates`
--
ALTER TABLE `templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `contacts_filters`
--
ALTER TABLE `contacts_filters`
  ADD CONSTRAINT `fk_contacts_filters_contacts` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_contacts_filters_filters` FOREIGN KEY (`filter_id`) REFERENCES `filters` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `notifications_filters`
--
ALTER TABLE `notifications_filters`
  ADD CONSTRAINT `fk_notifs_filters_filters` FOREIGN KEY (`filter_id`) REFERENCES `filters` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_notifs_filters_notifications` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
