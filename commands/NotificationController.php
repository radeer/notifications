<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\commands;

use yii\console\Controller;
use app\components\helpers\Inflector;
use domain\services\NotificationDispatcher;

/**
 * Description of NotificationController
 *
 * @author Rust
 */
class NotificationController extends Controller {
	
	private $notificationDispatcher;
	
	public function __construct($id, $module, NotificationDispatcher $notificationDispatcher, $config = array()) {
		parent::__construct($id, $module, $config);
		$this->notificationDispatcher = $notificationDispatcher;
	}
	
	/**
	 * php yii notification/send title1,title2,title3,...
	 * 
	 * Еесли название содержит пробелы, то нужно написать в верблюжьей нотации
	 * php yii notification/send названиеИзНесколькихСлов,ещеОдноНазвание
	 * 
	 * @param array $notificationTitles
	 */
	public function actionSend($notificationTitles) {
		if (count($notificationTitles) > 0) {
			foreach ($notificationTitles as &$title) {
				$title = Inflector::camel2id($title, ' ');
			}
		}
		$this->notificationDispatcher->notifyByTitles($notificationTitles);
	}
	
}
