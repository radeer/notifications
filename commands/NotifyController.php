<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\User;
use domain\entities\Contact\Contact;
use domain\services\mailing\EmailService;
use yii\console\Controller;
use yii\mail\BaseMailer;
use yii\swiftmailer\Mailer;
use yii\helpers\ArrayHelper;
/**
 * This command echoes the first argument that you have entered.
 * @param EmailService mailing\EmailService
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class NotifyController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {   $result = '';
        $query = Contact::find()->filterWhere(['like', 'birthday', date('m-d', strtotime("+1 days"))])->all();
        $array = ArrayHelper::map($query, 'id', 'client_name');
        if (count($array)>0) {
            foreach ($array as $key => $element) {
                if ($element) {

                    $result .= "<br><a href='http://qwe/web/contacts/view?id={$key}'>"  . $element . ' ' . '</a>';

                }
                unset($key);
                unset($element);
            }
            $body = "У следующих клиентов завтра день рождения: .$result<br>";
            $data = User::find()->select('email')->filterWhere(['not like', 'username', 'admin', false])->all();
            $users = ArrayHelper::getColumn($data, 'email');
            print_r($users);
            echo "\n";
            foreach ($users as $email) {
            //    EmailService::send($email, 'Напоиминание о дне рождения клиента компании!', $body, 'birthday');
                \Yii::$app->mailer->htmlLayout = 'layouts/birthday';
                \Yii::$app->mailer->compose(['html' => 'views/html'])
                    //->setFrom()
                    ->setTo($email)
                    ->setSubject('Напоиминание о дне рождения клиента компании!')
                    ->setHtmlBody($body)
                    ->send();


            }
            unset($email);
        }
        else echo "Завтра дней рождений нет (:";
    }


    public function actionGreet() {
        $body = "От лица компании KRONOSPAN поздравляем Вас с днём рождения!";
        $query = Contact::find()->filterWhere(['like', 'birthday', date('m-d')])->all();
        $array = ArrayHelper::map($query, 'email', 'client_name');
        if (count($array)>0) {
        foreach ($array as $email => $client) {
            \Yii::$app->mailer->htmlLayout = 'layouts/happybirthday';
            \Yii::$app->mailer->compose(['html' => 'views/html'])
                //->setFrom()
                ->setTo($email)
                ->setSubject('С днём рождения, '.$client .'!')
                ->setHtmlBody($body)
                ->send();
        }
        }
    }
}
