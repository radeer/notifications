<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'language' => 'en-EN',
    'sourceLanguage' => 'en-US',
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'layout' => 'basic',
    'on beforeAction'=>function($event){
        $cookies = Yii::$app->request->cookies;
        \Yii::$app->language = $language = $cookies->getValue('language', 'en-EN');
},
    'bootstrap' => [
		'log',
		'\domain\bootstrap\Bootstrap'
	],
	/*'controllerMap' => [
        'elfinder' => [
			'class' => 'mihaildev\elfinder\PathController',
			'access' => ['@', '?'],
			'root' => [
				'path' => 'files',
				'name' => 'Files'
			],
			'watermark' => [
						'source'         => __DIR__.'/logo.png', // Path to Water mark image
						 'marginRight'    => 5,          // Margin right pixel
						 'marginBottom'   => 5,          // Margin bottom pixel
						 'quality'        => 95,         // JPEG image save quality
						 'transparency'   => 70,         // Water mark image transparency ( other than PNG )
						 'targetType'     => IMG_GIF|IMG_JPG|IMG_PNG|IMG_WBMP, // Target image formats ( bit-field )
						 'targetMinPixel' => 200         // Target image minimum pixel size
			]
		]
    ], */
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
		'@domain' => '@app/domain',
    ],

    'components' => [
	
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'N7E8C3XDOmT_IsAId_2fVV-PkrEdmnX2',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
            'i18n' => [ // компонент мультизязычности
                    'translations' => [
                            'app*' => [
                                    'class' => 'yii\i18n\PhpMessageSource',
                                    'basePath' => '@domain/messages', // ВАЖНО! этот путь к папке с переводами
                                    'sourceLanguage' => 'en-US', // базовым языком путь будет инглиш
                                    'fileMap' => [
                                        'app' => 'app.php', // группа фраз и её файл-источниr
                                        'app/error' => 'error.php', // для ошибок (тоже какое-то подмножетсво переводимых фраз)
                                    ],
            ], ], ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache' //Включаем кеширование
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
			'viewPath' => '@app/mail',
			
			'htmlLayout' => 'layouts/l_html',
			'messageConfig' => [
				'charset' => 'UTF-8',
				'from' => [$params['from'] => $params['fromSite']]
			],
			'transport' => [
				'class' => 'Swift_SmtpTransport',
                'host' => 'ssl://smtp.yandex.com',
                'username' => $params['from'],
                'password' => $params['fromPassword'],
                'port' => '465',
			]
			
        ],
		
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
				'' => 'site/index'
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
		'generators' => [
			'crud' => [
				'class' => \app\components\gii\CrudGenerator::class,
			],
		]
    ];
}



return $config;
