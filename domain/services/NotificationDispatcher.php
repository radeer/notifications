<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\services;

use app\models\Organization;
use app\models\OrganizationFilter;
use app\models\TemplatesModel\TemplatesModel;
use domain\entities\Contact\Contact;
use domain\entities\Notification\NotificationFilter;
use Yii;
use yii\helpers\ArrayHelper;

use domain\services\mailing\SmsService;
use domain\services\mailing\EmailService;
use domain\entities\Notification\Notification;
use domain\repositories\NotificationRepository;
use domain\repositories\ContactRepository;
use domain\repositories\FilterRepository;
use yii\helpers\VarDumper;

/**
 * Description of NotificationDispatcher
 *
 * @author Rust
 */
class NotificationDispatcher {
	
	private $notificationRepository;
	private $contactRepository;
	private $filterRepository;
	private $emailService;
	private $smsService;

	
	/**
	 * 
	 * @param NotificationRepository $notificationRepository
	 * @param ContactRepository $contactRepository
	 * @param FilterRepository $filterRepository
	 * @param EmailService $emailService
	 * @param SmsService $smsService
	 */
	public function __construct(
			NotificationRepository $notificationRepository, 
			ContactRepository $contactRepository,
			FilterRepository $filterRepository,
			EmailService $emailService,
			SmsService $smsService			
	) {
		$this->notificationRepository = $notificationRepository;
		$this->contactRepository = $contactRepository;
		$this->filterRepository = $filterRepository;
		$this->emailService = $emailService;
		$this->smsService = $smsService;
		
	}
	
	/**
	 * 
	 * @param integer $id
	 */
	public function notifyById($id) {
		$notification = $this->notificationRepository->get($id);
		$this->notify($notification);
	}
	
	/**
	 * 
	 * @param string $filterTitle
	 */
	public function notifyByFilterTitle(string $filterTitle) {
		$filter = $this->filterRepository->getByTitle($filterName);
		$notifications = $this->notificationRepository->getByFilterId($filter->id);
		$this->notifyAll($notifications);
	}
	
	/**
	 * 
	 * @param string[] $filterTitles
	 */
	public function notifyByFilterTitles(array $filterTitles) {
		$filters = $this->filterRepository->getByTitles($filterTitles);
		if (count($filters) > 0) {
			foreach ($filters as $filter) {
				$notifications = $this->notificationRepository->getByFilterId($filter->id);
				$this->notifyAll($notifications);
			}
		}
	}
	
	/**
	 * 
	 * @param string[] $titles
	 */
	public function notifyByTitles(array $titles) {
		$notifications = $this->notificationRepository->getByTitles($titles);
		$this->notifyAll($notifications);
	}
	
	/**
	 * 
	 * @param Notification[] $notifications
	 */
	protected function notifyAll(array $notifications) {
		if (count($notifications) > 0) {
			foreach ($notifications as $notification) {
				$this->notify($notification);
			}
		}
	}
	
	/**
	 * 
	 * @param Notification $notification
	 */


    public function notifyByContactId ($direction, $id) {
        $notification = $this->notificationRepository->get($id);
        $q = TemplatesModel::find()->select(['template'])->where(['id' => $notification->id_template])->all();
        $nametemplate = ArrayHelper::getColumn($q, 'template');
        $this->emailService->send($direction, $notification->heading, $notification->content, implode($nametemplate));
    }

    public function notifyTest ($to, $id) {
        $notification = $this->notificationRepository->get($id);
        $q = TemplatesModel::find()->select(['template'])->where(['id' => $notification->id_template])->all();
        $nametemplate = ArrayHelper::getColumn($q, 'template');
        $this->emailService->send($to, $notification->heading, $notification->content,implode($nametemplate));

    }




    protected function notify(Notification $notification) {
        $session = Yii::$app->session;
        $nametemplate = $session->get('template');
        $heading = $notification->heading;
        $a = NotificationFilter::find()->where(['notification_id' => $notification->id])->all();
        $b = ArrayHelper::getColumn($a,'filter_id');
        $c = OrganizationFilter::find()->where(['filter_id' => $b])->all();
        $e = ArrayHelper::getColumn($c,'organization_id');
        $r = Contact::find()->where(['organization_id' => $e])->all();
        $contacts = ArrayHelper::map($r,'email', 'phone');
        $body = $notification->content;
                    if (count($contacts) >= 0) {
                        foreach ($contacts as $contact => $email) {
                            if ($notification->email) {
                                $this->emailService->send($contact, $heading, $body, $nametemplate);
                            }
                            if ($notification->sms) {
                                $this->smsService->send($email, $heading, $body, $nametemplate);
                            }
                        }
                    }
        $session->destroy();
        $session->close();
    }

}
