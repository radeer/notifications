<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\services\mailing;

use Yii;
use domain\services;
/**
 * Description of EmailService
 *
 * @author Rust
 */
class EmailService extends MailingService {
	
	/**
	 * 
	 * @param string $to	- email
	 * @param string $heading
	 * @param string $body
	 */
	public function send($to, $heading, $body, $nametemplate) {
		Yii::$app->mailer->getView()->params['content'] = $body;
		Yii::$app->mailer->htmlLayout = 'layouts/'.$nametemplate;
		Yii::$app->mailer->compose(['html' => 'views/html'])
			//->setFrom()
			->setTo($to)
			->setSubject($heading)
		//	->setTextBody($body)
			->send();
	}

}
