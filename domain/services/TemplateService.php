<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\services;

use Yii;

use domain\forms\Template\TemplateForm;
//use domain\repositories\ContactRepository;
use domain\repositories\FilterRepository;
use app\models\TemplatesModel;
/**
 * Description of ContactService
 *
 * @author Rust
 */
class TemplateService {
	
	private $templateRepository;
	
	
	/**
	 * 
	 * @param ContactRepository $contactRepository
	 * @param FilterRepository $filterRepository
	 */
	
	
	
	public function rebuild(TemplatesModel\TemplatesModel $form) {
		$session = Yii::$app->session;
		$session->open();
		$session['content'] = $form->content;
	if ($form->test !== "") {
		
		$session['to'] = $form->test;

		}
	if (($form->template=="") && ($form->customtemplate=="")) 
		{ Yii::$app->session->SetFlash(
						'error',
						'Выберите шаблон '.$form->template );

					} 
		else {
		if ((!$form->template=="") && (!$form->customtemplate=="")) 
			{
						Yii::$app->session->SetFlash(
						'error',
						'Необходимо выбрать один из варинтов: существующий шаблон или новый' );

			} 
		else	{ 
		if (!$form->customtemplate=="") {
			$nametemplate = $form->customtemplate;
			$text = $form->text;
			$this->dobuild($text, $nametemplate);
			$form->template = $nametemplate;
			$form->save();
			}
		else $nametemplate = $form->template;

		$session['template'] = $nametemplate;
		$session->close();
		
	}
	}
	
	}
	
	public function dobuild($text, $nametemplate) {
	if (!file_exists("../mail/layouts/". $nametemplate. ".php")) {
	$file = fopen("../mail/layouts/". $nametemplate. ".php", "w");
	fwrite($file, $text);
	fclose($file);
	} 
	}
	
	protected function populate(template &$template, TemplateForm $form) {
		$template->id = $form->id;
		$template->template = $form->template;
		$template->text = $form->text;
		
	}
	
}
