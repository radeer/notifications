<?php

namespace domain\entities\Contact;

use app\models\Organization;
use Yii;
use yii\helpers\ArrayHelper;

use domain\traits\InstantiateTrait;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $addressname
 * @property string $client_name
 * @property string $phone
 * @property string $email
 * @property int $organization_id
 * @property date $birthday
 * @property string type
 */
class Contact extends \yii\db\ActiveRecord
{
	
	use InstantiateTrait;
	
	public function __construct($addressname, $client_name, $organization_id, $phone, $email, $birthday, $type, $config = array()) {
		parent::__construct($config);
		$this->addressname = $addressname;
		$this->client_name = $client_name;
		$this->organization_id = $organization_id;
		$this->email = $email;
		$this->phone = $phone;
		$this->birthday = $birthday;
		$this->type = $type;
	}
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts';
    }

    public function rules()
    {
        return [
            [['client_name', 'organization_id'], 'required', 'message' => 'Поле не может быть пустым'],
            [['addressname'], 'string'],
            [['client_name', 'type'], 'string', 'max' => 255],
            [['phone', 'organization_id'], 'integer'],
            ['email', 'email'],
            ['birthday', 'date', 'format' => 'yyyy-MM-dd'],
            //[['type', 'string'], 'max' => 15],
        ];
    }

    public function attributeLabels()
    {
        return [
            'addressname' => Yii::t('app', 'Address'),
            'client_name' => Yii::t('app', 'Client name'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => 'Email',
            'birthday' => Yii::t('app', 'Birthday'),
            'type' => Yii::t('app', 'Type'),
        ];

    }

	public static function find() {
		return new ContactQuery(get_called_class());
	}




    public function getOrganizations() {
        return $this->hasMany(Organization::className(), ['id' => 'organization_id']);
    }

    public function getOrganization()
    {
        return $this->hasOne(Organization::class, ['id' => 'organization_id']);

    }
}
