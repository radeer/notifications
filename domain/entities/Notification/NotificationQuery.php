<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\entities\Notification;

use yii\db\ActiveQuery;
use domain\entities\Notification\NotificationFilter;

/**
 * Description of NotificationQuery
 *
 * @author Rust
 */
class NotificationQuery extends ActiveQuery {
	
	public function one($db = null) {
		return parent::one($db);
	}
	
	public function all($db = null) {
		return parent::all($db);
	}
	
	public function byFilterId($filterId) {
		$nft = NotificationFilter::tableName();
		return $this->joinWith('notificationFilters')
			->andWhere(["$nft.filter_id" => $filterId]);
	}
	
}
