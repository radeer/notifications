<?php

namespace domain\entities\Notification;

use Foo\DataProviderIssue2859\TestWithDataProviderTest;
use Yii;

use domain\traits\InstantiateTrait;
use domain\entities\Filter\Filter;
use app\models\TemplatesModel\TemplatesModel;

/**
 * This is the model class for table "notifications".
 *
 * @property int $id
 * @property string $title Название рассылки
 * @property string $heading Заголовок сообщения
 * @property string $content Тело сообщения
 * @property int $email
 * @property int $sms
 * @property int $id_template
 *
 * @property TemplatesModel $template
 *
 * @property NotificationsFilters[] $notificationsFilters
 */
class Notification extends \yii\db\ActiveRecord
{
	use InstantiateTrait;
	
	public function __construct($title, $config = array()) {
		parent::__construct($config);
		$this->title = $title;
	}
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications';
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => Yii::t('app', 'Subject'),
            'heading' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'email' => 'Email',
            'sms' => 'Sms',
            'filters' => Yii::t('app', 'Receivers'),
            'id_template' => Yii::t('app', 'Template'),
            'test' => Yii::t('app', 'Email for send test'),
            'contact' => Yii::t('app', 'Contact'),
        ];
    }



    public static function find() {
		return new NotificationQuery(get_called_class());
	}
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilters()
    {
        return $this->hasMany(Filter::class, ['id' => 'filter_id'])
			->via('notificationFilters');
    }
	
	public function getNotificationFilters() {
		return $this->hasMany(NotificationFilter::class, ['notification_id' => 'id']);
	}

    public function getTemplate()
    {
        return $this->hasOne(TemplatesModel::class, ['id' => 'id_template']);

    }

    public function getCurrentTemplate()
    {
       // $template = TemplatesModel::find()->where(['id' => notification.$this->id_template])->count();
        $template = $this->hasOne(TemplatesModel::class, ['id' => 'id_template'])->select(template)->one();
        return $template->template;

    }
	
}
