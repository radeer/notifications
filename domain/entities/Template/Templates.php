<?php

namespace \domain\entities\Template;
use yii\base\Model;
use Yii;

/**
 * This is the model class for table "templates".
 *
 * @property int $id
 * @property string $template
 *
 * @property Notifications[] $notifications
 */
class Templates extends Model
{
	public $text;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template'], 'required'],
            [['template'], 'string'],
			[['text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'template' => 'Template',
			'text' => 'Шаблон',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifications()
    {
        return $this->hasMany(Notifications::className(), ['id_template' => 'id']);
    }

    /**
     * @inheritdoc
     * @return TemplatesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TemplatesQuery(get_called_class());
    }
}
