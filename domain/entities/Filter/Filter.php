<?php

namespace domain\entities\Filter;

use app\models\Organization;
use Yii;
use domain\traits\InstantiateTrait;
/**
 * This is the model class for table "filters".
 *
 * @property int $id
 * @property string $title Название фильтра
 * @property string $description Описание
 *
 * @property OrganizationFilters[] $contactsFilters
 * @property Contacts[] $contacts
 * @property NotificationsFilters[] $notificationsFilters
 */
class Filter extends \yii\db\ActiveRecord
{
	use InstantiateTrait;
	
	public function __construct($title, $description = null, $config = array()) {
		parent::__construct($config);
		$this->title = $title;
		$this->description = $description;
	}
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filters';
    }


    public function attributeLabels()
    {
        return [
            'title' => Yii::t('app', 'Filter'),
            'description' => Yii::t('app', 'Segment'),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizationFilters()
    {
        return $this->hasMany(OrganizationFilters::className(), ['filter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizations()
    {
        return $this->hasMany(Organization::className(), ['id' => 'organization_id'])
            ->viaTable('organization_filters', ['filter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationsFilters()
    {
        return $this->hasMany(NotificationsFilters::className(), ['filter_id' => 'id']);
    }
}
