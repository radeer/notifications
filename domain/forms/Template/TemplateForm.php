<?php

namespace domain\forms\Template;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use app\models\TemplatesModel;


/**
 * This is the model class for table "templates".
 *
 * @property int $id
 * @property string $template
 * @property string $t_description
 * @property Notifications[] $notifications
 */
class TemplateForm extends Model
{	
	public $template;
	public $text;
	public $customtemplate;
	public $t_description;
    /**
     * @inheritdoc
     */
	 
	 
	public static function tableName() {
		return 'templates';
			
	}	
	 
   public function __construct(TemplateForm $template = null, $config = array()) {
		parent::__construct($config);
		
		if ($template !== null) {
			$this->_template = $template;
			$this->id = $template->id;
			$this->template = $template->template;
			$this->text = $template->text;
			$this->customtemplate = $template->customtemplate;
			$this->t_description = $template->t_description;
		
			}
		}
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template', 'customtemplate'], 'string'],
			[['text', 't_description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'template' => 'Имя шаблона',
			'text' => 'Шаблон',
			'customtemplate' => 'Имя нового шаблона',
            't_description' => 'Описание шаблона'
        ];
    }

  
}
