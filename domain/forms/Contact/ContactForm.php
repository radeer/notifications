<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace domain\forms\Contact;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use domain\entities\Contact\Contact;


/**
 * Description of ContactForm
 *
 * @author Rust
 */
class ContactForm extends Model {
	
	private $_contact;
	
	public $id;
	public $addressname;
	public $client_name;
	public $phone;
	public $email;
	public $birthday;
    public $organization_id;
    public $type;
	

	
	public function __construct(Contact $contact = null, $config = array()) {
		parent::__construct($config);
		if ($contact !== null) {
			$this->_contact = $contact;
			$this->id = $contact->id;
			$this->addressname = $contact->addressname;
			$this->client_name = $contact->client_name;
			$this->phone = $contact->phone;
			$this->email = $contact->email;
            $this->birthday = $contact->birthday;
            $this->organization_id = $contact->organization_id;
            $this->type = $contact->type;
		}
	}
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_name', 'organization_id'], 'required', 'message' => 'Поле не может быть пустым'],
            [['addressname'], 'string'],
            [['client_name', 'email'], 'string', 'max' => 255],
            [['phone', 'organization_id'], 'integer'],
			['email', 'email'],
            ['birthday', 'date', 'format' => 'yyyy-MM-dd'],
            [['type'], 'string', 'max' => '15'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'addressname' => Yii::t('app', 'Address'),
            'client_name' => Yii::t('app', 'Client name'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => 'Email',
            'birthday' => Yii::t('app', 'Birthday'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

}
