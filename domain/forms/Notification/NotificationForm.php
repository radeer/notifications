<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace domain\forms\Notification;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use domain\entities\Notification\Notification;
use domain\validators\FiltersValidator;

/**
 * Description of NotificationForm
 *
 * @author Rust
 */
class NotificationForm extends Model {
	
	private $_notification;
	
	public $id;
	public $title;
	public $heading;
	public $content;
	public $email;
	public $sms;
	public $id_template;
    public $test;
    public $contact;

	public $filters = [];
	
	public function __construct(Notification $notification = null, $config = array())
    {
        parent::__construct($config);
        $this->email = true;
        $this->sms = false;
        if ($notification !== null) {
            $this->_notification = $notification;
            $this->id = $notification->id;
            $this->title = $notification->title;
            $this->heading = $notification->heading;
            $this->content = $notification->content;
            $this->email = $notification->email;
            $this->sms = $notification->sms;
            $this->id_template = $notification->id_template;
          //  $this->contact = $notification->contact;
            if (count($notification->filters) > 0) {
                $this->filters = ArrayHelper::getColumn($notification->filters, 'id');

            }
        }
    }
	public function getFilterModels() {
		return $this->_notification->filters;
	}

  /*  public function getTemplates()
    {
        return $this->_notification->templates;
    } */
	/**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			['filters', FiltersValidator::class],
            [['title', 'id_template'], 'required'],
            [['content', 'contact'], 'string'],
            [['email', 'sms'], 'integer'],
            [['title', 'heading', 'test'], 'string', 'max' => 255],
            [['id_template'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => Yii::t('app', 'Subject'),
            'heading' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'email' => 'Email',
            'sms' => 'Sms',
		    'filters' => Yii::t('app', 'Receivers'),
            'id_template' => Yii::t('app', 'Template'),
            'test' => Yii::t('app', 'Email for send test'),
            'contact' => Yii::t('app', 'Contact'),
        ];
    }
	
}
