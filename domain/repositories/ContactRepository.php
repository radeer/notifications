<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\repositories;

use domain\entities\Contact\Contact;
use domain\entities\Contact\ContactFilter;
use domain\exceptions\NotFoundException;
use domain\exceptions\CantSaveException;
use domain\exceptions\CantDeleteException;
use domain\exceptions\IncorrectTypeException;

/**
 * Description of ContactRepository
 *
 * @author Rust
 */
class ContactRepository {
	
	/**
	 * 
	 * @param type $id
	 * @return Contact
	 * @throws NotFoundException
	 */
	public function get($id) {
		$contact = Contact::findOne(['id' => $id]);
		if ($contact === null) {
			throw new NotFoundException('Не удалось найти контакт по идентификатору ' . $id);
		}
		return $contact;
	}
	

	public function save(Contact $contact) {
		try {
			if (!$contact->save()) {
				throw new CantSaveException('Не удалось сохранить контакт');
			}
		} catch (\Exception $e) {
			throw new CantSaveException(
				'Не удалось сохранить контакт',
				$contact,
				0,
				$e
			);
		}
		return $contact->id;
	}
	
	public function delete(Contact $contact) {
		try {
			if (!$contact->delete()) {
				throw new CantDeleteException('Не удалось удалить контакт с идентификатором ' . $contact->id);
			}
		} catch (\Exception $e) {
			throw new CantDeleteException(
				'Не удалось удалить контакт с идентификатором ' . $contact->id,
				$contact,
				0,
				$e
			);
		}
	}
	
	/**
	 * 
	 * @param Contact $contact
	 * @throws \Exception
	 * @throws CantDeleteException
	 */

	
}
