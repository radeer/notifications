<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace domain\widgets;

use Yii;
use yii\base\Widget;
use yii\bootstrap\Alert;

/**
 * Description of NotificationAlert
 *
 * @author Rust
 */
class NotificationAlert extends Widget {
	
	public $error;
	public $successSended;
	public $successText = 'Рассылка успешно отправлена.';
	public $errorText = 'Не удалось отправить рассылку.';
	
	public function init() {
		parent::init();
		$this->error = null;
		if (Yii::$app->session->hasFlash('domainError')) {
			$this->error = Yii::$app->session->getFlash('domainError');
		}
		$this->successSended = null;
		if (Yii::$app->session->hasFlash('successNotification')) {
			$this->successSended = Yii::$app->session->getFlash('successNotification');
		}
	}
	
	public function run() {
		if ($this->successSended === null && $this->error == null) {
			return;
		}
		if ($this->successSended === null && $this->error !== null) {
			return $this->errorAlert($this->error->getMessage());
		}
		if ($this->successSended === false) {
			return $this->errorAlert();
		}
		return $this->successAlert($this->successText);
	}
	
	protected function errorAlert($text = 'Произошла непредвиденная ошибка') {
		return Alert::widget([
				'options' => [
					'class' => 'alert-danger'
				],
				'body' => $this->errorText . ' ' . $text
		]);
	}
	
	protected function successAlert($text) {
		return Alert::widget([
				'options' => [
					'class' => 'alert-success'
				],
				'body' => $text
			]);
	}
	
}
