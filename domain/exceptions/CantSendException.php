<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\exceptions;

use yii\base\Exception;

/**
 * Description of CantSendException
 *
 * @author Rust
 */
class CantSendException extends Exception {
	
	protected $message = 'Не удалось отправить сообщение!';
	
	public function getName()
    {
        return 'CantSendException';
    }
}
