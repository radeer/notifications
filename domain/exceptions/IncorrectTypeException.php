<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace domain\exceptions;

/**
 * Description of IncorrectTypeException
 *
 * @author Rust
 */
class IncorrectTypeException extends \Exception {
	
	protected $message = 'Неверный формат переменной!';
	
	public function getName()
    {
        return 'IncorrectTypeException';
    }
	
}
