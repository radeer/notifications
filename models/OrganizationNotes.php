<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organization_notes".
 *
 * @property int $id
 * @property string $note
 * @property int $organization_id
 * @property int $user_id
 * @property string $createdAt
 *
 * @property Organization $organization
 * @property User $user
 */
class OrganizationNotes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organization_notes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['note', 'organization_id', 'user_id'], 'required'],
            [['note'], 'string'],
            ['note', 'unique', 'message' => 'Такая заметка уже есть!'],
            [['organization_id', 'user_id'], 'integer'],
            [['createdAt'], 'safe'],
            [['organization_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::className(), 'targetAttribute' => ['organization_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'note' => Yii::t('app', 'Note'),
            'organization_id' => Yii::t('app', 'Organization ID '),
            'user_id' => Yii::t('app', 'User ID'),
            'createdAt' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganization()
    {
        return $this->hasOne(Organization::className(), ['id' => 'organization_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return OrganizationNotesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrganizationNotesQuery(get_called_class());
    }
}
