<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Organization;

/**
 * OrganizationSearch represents the model behind the search form of `app\models\Organization`.
 */
class OrganizationSearch extends Organization
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'associationnumber', 'mainassociationnumber'], 'integer'],
            [['Match_Code', 'mainaddress_adressname', 'country', 'zipcode', 'city', 'clerk_staffId', 'clerk_name', 'agent_name', 'isoCode', 'top_customer', 'Accounting_clerk_staffId', 'Accounting_clerk_name', 'Common_Match_Code', 'mainaddress_addressname2', 'deliveryaddress_addressname', 'deliveryaddress_city', 'deliveryaddress_phone', 'deliveryaddress_fax', 'deliveryaddress_mobilephone', 'deliveryaddress_email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Organization::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'associationnumber' => $this->associationnumber,
            'mainassociationnumber' => $this->mainassociationnumber,
        ]);

        $query->andFilterWhere(['like', 'Match_Code', $this->Match_Code])
            ->andFilterWhere(['like', 'mainaddress_adressname', $this->mainaddress_adressname])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'zipcode', $this->zipcode])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'clerk_staffId', $this->clerk_staffId])
            ->andFilterWhere(['like', 'clerk_name', $this->clerk_name])
            ->andFilterWhere(['like', 'agent_name', $this->agent_name])
            ->andFilterWhere(['like', 'isoCode', $this->isoCode])
            ->andFilterWhere(['like', 'top_customer', $this->top_customer])
            ->andFilterWhere(['like', 'Accounting_clerk_staffId', $this->Accounting_clerk_staffId])
            ->andFilterWhere(['like', 'Accounting_clerk_name', $this->Accounting_clerk_name])
            ->andFilterWhere(['like', 'Common_Match_Code', $this->Common_Match_Code])
            ->andFilterWhere(['like', 'mainaddress_addressname2', $this->mainaddress_addressname2])
            ->andFilterWhere(['like', 'deliveryaddress_addressname', $this->deliveryaddress_addressname])
            ->andFilterWhere(['like', 'deliveryaddress_city', $this->deliveryaddress_city])
            ->andFilterWhere(['like', 'deliveryaddress_phone', $this->deliveryaddress_phone])
            ->andFilterWhere(['like', 'deliveryaddress_fax', $this->deliveryaddress_fax])
            ->andFilterWhere(['like', 'deliveryaddress_mobilephone', $this->deliveryaddress_mobilephone])
            ->andFilterWhere(['like', 'deliveryaddress_email', $this->deliveryaddress_email]);

        return $dataProvider;
    }
}
