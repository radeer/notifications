<?php

namespace app\models;

use app\models\Organization;
use Yii;
use domain\entities\Filter\Filter;
use domain\traits\InstantiateTrait;

/**
 * This is the model class for table "contacts_filters".
 *
 * @property int $id
 * @property int $organization_id
 * @property int $filter_id
 *
 * @property Filters $filter
 * @property Organization $organization
 */
class OrganizationFilter extends \yii\db\ActiveRecord
{
	use InstantiateTrait;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organization_filters';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organization_id', 'filter_id'], 'required'],
            [['organization_id', 'filter_id'], 'integer'],
            [['organization_id', 'filter_id'], 'unique', 'targetAttribute' => ['organization_id', 'filter_id']],
            [['filter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Filter::className(), 'targetAttribute' => ['filter_id' => 'id']],
            [['organization_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::className(), 'targetAttribute' => ['organization_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'organization_id' => 'Organization ID',
            'filter_id' => 'Filter ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilter()
    {
        return $this->hasOne(Filter::className(), ['id' => 'filter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganization()
    {
        return $this->hasOne(Organization::className(), ['id' => 'contact_id']);
    }
}
