<?php

namespace app\models\TemplatesModel;

use Yii;
use yii\web\UploadedFile;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use domain\entities\Notification\Notification;
/**
 * This is the model class for table "templates".
 *
 * @property int $id
 * @property string $template
 * @property string $t_description
 * @property Notifications[] $notifications
 * @property Notifications $id0
 */
class TemplatesModel extends ActiveRecord
{	
	private $_template;
	public $text;
	public $customtemplate;
	public $test;
	public $content;

    /**
     * @inheritdoc
     */
    /**
     * @var UploadedFile|Null file attribute
     */
    public $file;
		 
	public static function tableName() {
		return 'templates';
			
	}	
	 
   public function __construct(TemplatesModel $template = null, $config = array()) {
		parent::__construct($config);
		
		if ($template !== null) {
			$this->_template = $template;
			$this->id = $template->id;
			$this->template = $template->template;
			$this->text = $template->text;
			$this->customtemplate = $template->customtemplate;
			$this->test = $template->test;

			}
		}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template', 'customtemplate'], 'string'],
			[['text', 'test'], 'string'],
			[['content', 't_description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'template' => Yii::t('app', 'Name of file'),
			'text' => Yii::t('app', 'Template'),
			'customtemplate' => Yii::t('app', 'Name of new template'),
			'test' => Yii::t('app', 'Email for send test'),
			'content' => Yii::t('app', 'Content'),
            't_description' => Yii::t('app', 'Description'),
        ];
    }





	public function findtemplates($from) {
		  $templates = TemplatesModel::find()->all();
		  if ($from == 'templates') {
		  $items = ArrayHelper::map($templates,'template','template'); }
		  else  $items = ArrayHelper::map($templates,'template','t_description');
		  return $items;
	}


	public function findonetemp($id)
    {
        $templates = TemplatesModel::find()->where(['id' => $id])->all();
        $items = ArrayHelper::map($templates,'id','template');
        return $items;
    }



    public function findcurrenttemp($id)
    {
        $field = Notification::find()->select('id_template')->where(['id' => $id])->all();
        $field = ArrayHelper::map($field, 'id_template', 'id_template');
        $field = implode($field);
        $templates = TemplatesModel::find()->where(['id' => $field])->all();
        $items = ArrayHelper::map($templates,'id','template');
        return implode($items);
    }


    public function gethtml($id) {
        $namefile = TemplatesModel::find()->select('template')->where(['id' => $id])->all();
        $result = ArrayHelper::map($namefile,'template','template');
        return $result;

    }

    public function findtempbyname($template)
    {
        $templates = TemplatesModel::find()->select('id')->where(['template' => $template]);
        return $templates;
    }
	
	 public function getNotifications()
    {
        return $this->hasMany(Notification::className(), ['id_template' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {

        return $this->hasOne(Notification::className(), ['id_template' => 'id']);
    }

  
}
