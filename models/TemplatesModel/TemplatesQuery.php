<?php

namespace app\models\TemplatesModel;

/**
 * This is the ActiveQuery class for [[TemplatesModel]].
 *
 * @see TemplatesModel
 */
class TemplatesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TemplatesModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TemplatesModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
