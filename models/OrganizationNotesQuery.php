<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[OrganizationNotes]].
 *
 * @see OrganizationNotes
 */
class OrganizationNotesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return OrganizationNotes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OrganizationNotes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
