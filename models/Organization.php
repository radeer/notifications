<?php

namespace app\models;

use domain\entities\Contact\Contact;
use domain\entities\Filter\Filter;
use domain\forms\FilterForm;
use domain\services\ContactService;
use Yii;
use domain\validators\FiltersValidator;
use \domain\exceptions\IncorrectTypeException;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "organization".
 *
 * @property int $id
 * @property string $Match_Code
 * @property string $mainaddress_adressname
 * @property string $country
 * @property string $zipcode
 * @property string $city
 * @property string $clerk_staffId
 * @property string $clerk_name
 * @property int $associationnumber
 * @property int $mainassociationnumber
 * @property string $agent_name
 * @property string $isoCode
 * @property int $top_customer
 * @property string $Accounting_clerk_staffId
 * @property string $Accounting_clerk_name
 * @property string $Common_Match_Code
 * @property string $mainaddress_addressname2
 * @property string $deliveryaddress_addressname
 * @property string $deliveryaddress_city
 * @property string $deliveryaddress_phone
 * @property string $deliveryaddress_fax
 * @property string $deliveryaddress_mobilephone
 * @property string $deliveryaddress_email
 * @property Contacts[] $contacts
 * @property Filters[] $filters
 */
class Organization extends \yii\db\ActiveRecord
{
    public $filters = [];
    public $excelfile = null;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organization';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['filters', FiltersValidator::class],
            [['Match_Code', 'mainaddress_adressname', 'country', 'zipcode'], 'safe'],
            [['excelfile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xlsx, csv'],
            [['associationnumber', 'mainassociationnumber'], 'integer'],
            [['Match_Code', 'mainaddress_adressname', 'country', 'zipcode', 'city', 'clerk_staffId', 'clerk_name', 'agent_name', 'isoCode', 'Accounting_clerk_staffId', 'Accounting_clerk_name', 'Common_Match_Code', 'mainaddress_addressname2', 'deliveryaddress_addressname', 'deliveryaddress_city', 'deliveryaddress_phone', 'deliveryaddress_fax', 'deliveryaddress_mobilephone'], 'string', 'max' => 255],
            [['top_customer'], 'boolean'],
            ['deliveryaddress_email', 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Match_Code' => 'Match  Code',
            'mainaddress_adressname' => Yii::t('app', 'Name organizaton'),
            'country' => Yii::t('app', 'Country'),
            'zipcode' => Yii::t('app', 'Zipcode'),
            'city' => Yii::t('app', 'City'),
            'clerk_staffId' => 'Clerk Staff ID',
            'clerk_name' => Yii::t('app', 'Clerk name'),
            'associationnumber' => 'Associationnumber',
            'mainassociationnumber' => 'Mainassociationnumber',
            'agent_name' => Yii::t('app', 'Agent name'),
            'isoCode' => Yii::t('app', 'isoCode'),
            'top_customer' => Yii::t('app', 'Top customer'),
            'Accounting_clerk_staffId' => 'Accounting Clerk Staff ID',
            'Accounting_clerk_name' => 'Accounting Clerk Name',
            'Common_Match_Code' => 'Common  Match  Code',
            'mainaddress_addressname2' => 'Mainaddress Addressname2',
            'deliveryaddress_addressname' => 'Deliveryaddress Addressname',
            'deliveryaddress_city' => Yii::t('app', 'Delivery city'),
            'deliveryaddress_phone' => Yii::t('app', 'Delivery phone'),
            'deliveryaddress_fax' => Yii::t('app', 'Delivery fax'),
            'deliveryaddress_mobilephone' => Yii::t('app', 'Delivery mobile'),
            'deliveryaddress_email' => Yii::t('app', 'Delivery email'),
            'excelfile' => Yii::t('app', 'Excel file'),
            'createdAt' => Yii::t('app', 'Created at'),
            'filters' => Yii::t('app', 'Segments'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function upload()
    {
        if ($this->validate()) {
            $str = uniqid();
            $this->excelfile->name = $str . '.' . $this->excelfile->extension;
            $this->excelfile->saveAs('../excelupload/' . $this->excelfile->name);


        return "Done!";
        } else {
        return "Error!";
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizationFilters()
    {
        return $this->hasMany(OrganizationFilter::class, ['organization_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilterss()
    {
        return $this->hasMany(Filter::class, ['id' => 'filter_id'])
            ->viaTable('organization_filters', ['organization_id' => 'id'])->asArray();
    }

    public function getContacts()
    {
        return $this->hasMany(Contact::className(), ['organization_id' => 'id']);
    }

    public function getOrganizations() {

        return Organization::find()->all();

    }


    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if (!$this->isNewRecord) {
            OrganizationFilter::deleteAll(['organization_id' => $this->id]);
        }
            foreach ($this->filters as $filter) {
                $form = new OrganizationFilter();
                $form->organization_id = $this->id;
                $form->filter_id = $filter;
                $form->save(false);
            }

            /*    else {

                    foreach ($this->filters as $filter)
                    {
                        $form = new OrganizationFilter();
                        $form->organization_id = $this->id;
                        $form->filter_id = $filter;
                        $form->save(false);
                    }
           */
        }




    public function getByFilterIds(array $filterIds) {
        $organizations = [];
        if (count($filterIds) > 0) {
            $ids = [];
            foreach ($filterIds as $key => $id) {
                if (!is_numeric($id)) {
                    throw new IncorrectTypeException('Идентификатор фильтра должен быть целым числом!');
                }
                $ids[] = $id;
            }
            $organizations = Organization::find()->byFilterIds($ids)->all();
        }
        return $organizations;
    }
}
