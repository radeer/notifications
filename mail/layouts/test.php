<?php

/* @var $this \yii\web\View */
/* @var $content string */


use yii\helpers\Html;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="<?= Yii::$app->charset ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<?php $this->head() ?>
		<link rel="stylesheet" href="style.css">
		<?php $this->head() ?>
	</head>
		<body style="background-color: rgb(240, 240, 240);">
		<?php $this->beginBody() ?>
			<div style="width: 600px;
margin: auto;
background-color: white;
margin-top: 25px;">
				<div style="display: flex; justify-content: center; align-items: center; padding-top: 10px; padding-bottom: 25px;">
					<img src="https://resize.yandex.net/mailservice?url=http%3A%2F%2Fi8.createsend1.com%2Fei%2Fi%2F70%2F021%2FA88%2F023930%2Fimages%2Fkronospan.png&proxy=yes&key=e0b1ffb87a7991d7ad5ff2556e1796e4"></img>
				</div>
			<div style="display: flex; justify-content: center; align-items: center;">
				<img alt="" width="100" height="30" border="0" src="https://resize.yandex.net/mailservice?url=http%3A%2F%2Fi9.createsend1.com%2Fei%2Fi%2F70%2F021%2FA88%2F023930%2Fimages%2Fproducts_100.png&amp;proxy=yes&amp;key=b407c5cf98b74e1c0dd4c88696573808">
				<img alt="" width="100" height="30" border="0" src="https://resize.yandex.net/mailservice?url=http%3A%2F%2Fi6.createsend1.com%2Fei%2Fi%2F70%2F021%2FA88%2F023930%2Fimages%2Fdecors_100.png&amp;proxy=yes&amp;key=6d5d0cf30048a02c81247a2ff7a50062">
				<img alt="" width="100" height="30" border="0" src="https://resize.yandex.net/mailservice?url=http%3A%2F%2Fi7.createsend1.com%2Fei%2Fi%2F70%2F021%2FA88%2F023930%2Fimages%2Fhighlights_100.png&amp;proxy=yes&amp;key=9f4b1f7998d9ca78f6e678e871a7dbec">
			</div>
					<br><br><br><br><br><br><br>
					<?php $this->beginBody() ?>

					<?= $content ?>
					<br><br><br><br><br><br><br><br><br><br>
			<div style="font-family: Helvetica Neues,Helvetica,Arial,sans-serif;
font-size: 15px;
padding-bottom: 13px;
color: #000000;
font-weight: normal;
padding: 25px;">	
				<p>Наша новая коллекция Тренды 16/17 – это идеальное сочетание ДЕРЕВА, КАМНЯ и МЕТАЛЛА. Эта коллекция дополняет широчайший ассортимент Кроношпан 
				современными древесными декорами - 11 видов, брашированными металлами - 6 видов и столешницами - 10 видов.<p>
				<p>Вдохновленные самыми последними трендами дизайна, мы гордимся введением 27 новых декоров в 3 сочетаниях: Скандинавский (Nordic), Элитный 
				(Luxury) и Городской (Metropolitan).</p>

				<p>И они доступны на складе СЕЙЧАС!</p>


				<p>Для получения дополнительной информации и образцов, пожалуйста, обратитесь в ближайшее представительство Кроношпан или посетите сайт:<br>
				<a href="www.kronospan-express.com">www.kronospan-express.com</a> </p>
			</div>
			<div style="display: flex;
justify-content: center; 
align-items: center; 
">
				<img alt="" width="130" height="31" border="0" src="https://resize.yandex.net/mailservice?url=http%3A%2F%2Fi2.createsend1.com%2Fei%2Fi%2F70%2F021%2FA88%2F023930%2Fimages%2Fkronodesign.png&amp;proxy=yes&amp;key=eb13a17c8d328506969f4c2e9f6b2873">
				<img alt="" width="130" height="31" border="0" src="https://resize.yandex.net/mailservice?url=http%3A%2F%2Fi1.createsend1.com%2Fei%2Fi%2F70%2F021%2FA88%2F023930%2Fimages%2Fkronobuild.png&amp;proxy=yes&amp;key=956f33c15d7d33024387568df2f5b5c1">
				<img alt="" width="130" height="31" border="0" src="https://resize.yandex.net/mailservice?url=http%3A%2F%2Fi3.createsend1.com%2Fei%2Fi%2F70%2F021%2FA88%2F023930%2Fimages%2Fkronoart.png&amp;proxy=yes&amp;key=aa27be9c980598a60d2de498bf124720">
				<img alt="" width="130" height="31" border="0" src="https://resize.yandex.net/mailservice?url=http%3A%2F%2Fi4.createsend1.com%2Fei%2Fi%2F70%2F021%2FA88%2F023930%2Fimages%2Fkrono-original_130.png&amp;proxy=yes&amp;key=cbb257dc48f5e10cd4b47dcf28b418fb">
			</div>	
			<div style="display: flex;
justify-content: center; 
align-items: center; 
margin-top: 25px;">
				<img style="padding: 0;" alt="" width="30" height="30" border="0" src="https://resize.yandex.net/mailservice?url=http%3A%2F%2Fi5.createsend1.com%2Fei%2Fi%2F70%2F021%2FA88%2F023930%2Fimages%2Ffacebook.png&amp;proxy=yes&amp;key=7aa11faede9c1304a30382ba3e4784d1">
				<img alt="" style="padding-left: 45px;" width="30" height="30" border="0" src="https://resize.yandex.net/mailservice?url=http%3A%2F%2Fi6.createsend1.com%2Fei%2Fi%2F70%2F021%2FA88%2F023930%2Fimages%2Finstagram.png&amp;proxy=yes&amp;key=9d89c7f208db7ce2800aa14b12b9e5c7">
				<img alt="" style="padding-left: 45px;" width="30" height="30" border="0" src="https://resize.yandex.net/mailservice?url=http%3A%2F%2Fi7.createsend1.com%2Fei%2Fi%2F70%2F021%2FA88%2F023930%2Fimages%2Fpinterest.png&amp;proxy=yes&amp;key=e149728fc1f1ada4e461c14391323e9a">	
				<img alt="" width="30" style="padding-left: 45px;"  height="30" border="0" src="https://resize.yandex.net/mailservice?url=http%3A%2F%2Fi8.createsend1.com%2Fei%2Fi%2F70%2F021%2FA88%2F023930%2Fimages%2Fyoutube.png&amp;proxy=yes&amp;key=bf46252c8bd96d227b4fcd93eaed43d2">
			</div>
			<div style="background-color: rgb(242, 242, 242);
    width: 600px;
	margin: auto;

	color: #a6a6a6;
    font-size: 12px;
    font-family: Helvetica,Arial,sans-serif;
    margin-top: 0;
    margin-bottom: 15px;
    padding-top: 0;
    padding-bottom: 0;
    line-height: 18px;
    text-align: center;">
				<div style="text-align: -webkit-center;
background-color: rgb(215, 215, 215);
width: 600px;
color: #000000;
font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
font-size: 10px;
line-height: 11px;
margin-top: 0;
margin-bottom: 0;
padding-top: 2px;
padding-bottom: 2px;
font-weight: normal;
margin: auto;
margin-top: 45px;">
					<strong><a title="УСЛОВИЯ И ПОЛОЖЕНИЯ" href="http://kronodesigneood.createsend1.com/t/i-l-dkjylid-kujtuukjh-x/">УСЛОВИЯ И ПОЛОЖЕНИЯ</a> | <a title="ПОЛИТИКА КОНФИДЕНЦИАЛЬНОСТИ" href="http://kronodesigneood.createsend1.com/t/i-l-dkjylid-kujtuukjh-m/">ПОЛИТИКА КОНФИДЕНЦИАЛЬНОСТИ</a><br>
					© 2016 KRONOSPAN </strong>
					
				</div>
				<p>Вы получили это электронное сообщение, потому что Вы являетесь клиентом компании Кроношпан, или Вы зарегистрированы через интернет-сайт<p>
				<p><a style="color: #a6a6a6;" href="http://kronodesigneood.createsend1.com/t/i-l-dkjylid-kujtuukjh-c/">www.kronospan-express.com .</a></p>
				<p>Не забудьте добавить адрес <a style="color: #a6a6a6;" href="mailto:newsletter@krononews.com">newsletter@krononews.com</a> в Вашу адресную книгу, таким образом мы сможем снабжать Вас последней актуа льной информацией и предложениями. </p>
			</div>
			</div>
			<?php $this->endBody() ?>
		</body>
</html>
<?php $this->endPage() ?>