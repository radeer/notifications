<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" data-livestyle-extension="available"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<title>Супер предложение</title>
<style type="text/css"> 
.Email html
{
	width: 100%;
}

.Email ::-moz-selection{background:#fefac7;color:#4a4a4a;}
.Email ::selection{background:#fefac7;color:#4a4a4a;}

.Email body { 
   background-color: #f1f1f1; 
   margin: 0; 
   padding: 0; 
}

.Email .ReadMsgBody
{
	width: 100%;
	background-color: #f1f1f1;
}
.Email .ExternalClass
{
	width: 100%;
	background-color: #f1f1f1;
}

.Email a { 
    color:rgb(70, 67, 68); 
	text-decoration:none;
	font-weight:normal;
	font-style: normal;
} 
.Email a:hover { 
    color:#4a4a4a; 
	text-decoration:none;
	font-weight:normal;
	font-style: normal;
	transition: all .25s ease-in-out;
	
}

.Email a.heading-link {
	text-decoration:none;
	font-weight:bold;
	font-style: normal;
}

.Email a.heading-link:hover {
	text-decoration:none;
	font-weight:bold;
	font-style: normal;
}



.Email p,
.Email div {
	margin: 0 !important;
}
.Email table {
	border-collapse: collapse;
}


@media only screen and (max-width: 640px)  {
	body { width: auto !important; padding: 0px 20px !important;}
	body table table{width:100% !important; }
	body table[class="ds-bg-element"] {width: 100% !important; margin: 0 auto !important;}
	body table[class="table-inner"] {width: 100% !important; margin: 0 auto !important;}
	body table[class="full"] {width: 100% !important; margin: 0 auto !important;}
	body td[class="center"] {text-align: center !important;}
	body td[class="rewrite_padding"] {padding:30px !important;}

	body img[class="img_scale"] {width: 100% !important; height: auto !Important;}
	
}


@media only screen and (max-width: 479px)  {
	body { width: auto !important; padding: 0px 20px !important;}
	body table table{width:100% !important; }
	body table[class="ds-bg-element"] {width: 100% !important; margin: 0 auto !important;}
	body table[class="table-inner"] {width: 100% !important; margin: 0 auto !important;}
	body table[class="full"] {width: 100% !important; margin: 0 auto !important;}
	body td[class="center"] {text-align: center !important;}
	body td[class="rewrite_padding"] {padding:30px !important;}

	body img[class="img_scale"] {width: 100% !important; height: auto !Important;}
	
}

</style>
</head>
<body class="Email" bgcolor="#f1f1f1" style="margin: 0px; padding: 0px;">
<!-- START OF PRE HEADER TEXT-->
<table data-templateapp="Pre header text" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#f1f1f1" class="ds-bg-module" style="padding: 0;margin: 0;border-collapse: collapse;">
	<tbody><tr>
		<td valign="top" align="center">
			<table class="ds-bg-element" width="640" border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
				<tbody><tr>
					<td mc:edit="pre_header" align="center" style="padding: 30px 0px; margin: 0; font-weight: normal; font-size:13px ; color:#464344; font-family: &#39;Muli&#39;, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">
						<multiline><span>
						</span></multiline>
					</td>
				</tr>
			</tbody></table>
		</td>
	</tr>
</tbody></table>

<!-- START OF HEADER FOR LOGO-->
<table data-templateapp="Header for logo" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#f1f1f1" class="ds-bg-module" style="padding: 0;margin: 0;border-collapse: collapse;">
	<tbody><tr>
		<td valign="top" align="center">
			<table class="ds-bg-element" bgcolor="#ffffff" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
				<tbody><tr>
					<td width="640" style="padding: 0px 20px;">
						<table class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
							<tbody><tr>
								<td valign="top">
									<table class="full" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
										<tbody><tr>
											<td mc:edit="logo" align="center" style="padding: 40px 0px; margin: 0; font-weight: normal; font-size:13px ; color:#464344; font-family: &#39;Muli&#39;, sans-serif; line-height: 23px;">
												<span>
												<a href="http://mysite.ru" style="color: rgb(70, 67, 68);text-decoration: none;font-weight: normal;font-style: normal;">
													<img src="https://files.carrotquest.io/messenger/1453889993215-a9c59b28-1df1-478e-b5b4-5d555f7f695e.png" alt="logo" editable="True" width="70" height="70" border="0" style="display: inline-block;">
												</a>
												</span>
											</td>
										</tr>
									</tbody></table>
								</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
			</tbody></table>
		</td>
	</tr>
</tbody></table>
<!-- END OF HEADER FOR LOGO-->

<table data-templateapp="Left promo box right product" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#f1f1f1" class="ds-bg-module" style="padding: 0; margin: 0;">
	<tbody><tr>
		<td valign="top" align="center">
			<table class="ds-bg-element" bgcolor="#ffffff" width="640" border="0" align="center" cellpadding="0" cellspacing="0">
				<tbody><tr>
					<td width="640" style="padding: 0px 20px;">
						<table class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
							<tbody><tr>
								<td valign="top">
									<table class="full ds-bg-element" width="290" bgcolor="#464344" border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
										<tbody><tr>
											<td style="padding: 20px;">
												<table border="0" cellpadding="0" cellspacing="0">
													<!-- START OF HEADING-->
													<tbody><tr class="ds-remove">
														<td mc:edit="heading_PBRP" align="left" style="font-weight: normal; font-size:24px ; color:#ffffff; font-family: 'Muli', sans-serif; line-height: 34px;mso-line-height-rule: exactly;">
															<multiline><span>
														
															</span></multiline>
														</td>
													</tr>
													<!-- END OF HEADING-->
													
													<!-- START OF SUB HEADING-->
													<tr class="ds-remove">
														<td mc:edit="sub_heading_PBRP" align="left" style="padding-top:3px; font-weight: normal; font-size:36px ; color:#ffffff; font-family: 'Muli', sans-serif; line-height: 46px;mso-line-height-rule: exactly;">
															<multiline><span>
															45% Скидка!
															</span></multiline>
														</td>
													</tr>
													<!-- END OF SUB HEADING-->
													
													<!-- START OF TEXT-->
													<tr class="ds-remove">
														<td id="content" mc:edit="text_PBRP" align="left" style="padding-top: 15px; font-weight: normal; font-size:14px ; color:#ffffff; font-family: 'Muli', sans-serif; line-height: 24px;mso-line-height-rule: exactly;">
															<multiline><span>
															<?= $content ?>
															</span></multiline>
														</td>
													</tr>
													<!-- END OF TEXT-->
													
													<!-- START OF BUTTON-->
													<tr class="ds-remove">
														<td align="left" valign="top" style="padding-top: 20px; ">
															<table border="0" align="left" cellpadding="0" cellspacing="0" style="margin: 0;">
																<tbody><tr>
																	<td mc:edit="button_PBRP" align="center" valign="middle" style="border: 2px solid #ffffff; display: block; padding: 10px 20px; text-transform: uppercase; font-weight: normal; font-size: 13px; line-height: 18px; font-family: 'Muli', Arial, sans-serif; color:#ca9146; margin: 0 !important; mso-line-height-rule: exactly;">
																		<multiline><span>
																			<a href="#" style="text-decoration: none; font-style: normal; font-weight: normal; color:#ffffff;">
																			Посмотреть
																			</a>
																		</span></multiline>
																	</td>
																</tr>
															</tbody></table>
														</td>
													</tr>
													<!-- END OF BUTTON-->
												</tbody></table>
											</td>
										</tr>
									</tbody></table>
									
									<!-- START OF SPACER-->
									<table width="1" border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
										<tbody><tr>
											<td width="100%" height="20" style="line-heigh:0;">									
											&nbsp;
											</td>
										</tr>
									</tbody></table>
									<!-- START OF SPACER-->
									
									<table class="full" width="290" border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
										<!-- START OF IMAGE-->
										<tbody><tr>
											<td mc:edit="img_PBRP" align="center" style="margin: 0; padding: 0; font-size:14px ; color:#636363; font-family: 'Muli', Arial, sans-serif; line-height: 0px;">
												<span>
												<a href="#" style="color:#1dc8e9;">
												<img class="img_scale" src="https://files.carrotquest.io/messenger/1453886598543-6836a36d-2fb5-48f9-b7c5-d8427ae85902.png" editable="True" alt="img-290-320-3" width="290" height="320" border="0" style="display: block;" naptha_cursor="text">
												</a>
												</span>
											</td>
										</tr>
										<!-- END OF IMAGE-->
									</tbody></table>
								</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
				
				<!-- START OF VERTICAL SPACER-->
				<tr>
					<td height="20" style="padding:0; line-height: 0;">
					&nbsp;
					</td>
				</tr>
				<!-- END OF VERTICAL SPACER-->
			</tbody></table>
		</td>
	</tr>
</tbody></table>

<!-- START OF SOCIAL BLOCK-->
<table data-templateapp="Social block" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#f1f1f1" class="ds-bg-module" style="padding: 0;margin: 0;border-collapse: collapse;">
	<tbody><tr>
		<td valign="top" align="center">
			<table class="ds-bg-element" bgcolor="#e9e9e9" width="640" border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
				<tbody><tr>
					<td width="640" style="padding: 20px;">
						<table class="table-inner" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
							<tbody><tr>
								<td valign="top">
									<!-- START OF LEFT COLUMN-->
									<table class="full" width="290" border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
										<tbody><tr>
											<td mc:edit="left_column_social" class="center" align="left" style="text-transform: uppercase; padding-top: 4px; font-weight: normal; font-size:18px ; color:#464344; font-family: &#39;Muli&#39;, sans-serif; line-height: 28px;mso-line-height-rule: exactly;">
												<multiline><span>
												Мы в соц сетях
												</span></multiline>
											</td>
										</tr>
									</tbody></table>
									<!-- END OF LEFT COLUMN-->
									
									<!-- START OF SPACER-->
									<table width="1" border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
										<tbody><tr>
											<td width="100%" height="20" style="line-heigh:0;">									
											&nbsp;
											</td>
										</tr>
									</tbody></table>
									<!-- START OF SPACER-->
									
									<!-- START OF RIGHT COLUMN-->
									<table class="full" width="290" border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
										<tbody><tr>
											<td mc:edit="right_social" class="center" align="right" style="margin: 0; font-size:14px ; color:#636363; font-family: &#39;Muli&#39;, Arial, sans-serif; line-height: 0px;">
												<span>
												<a href="https://www.facebook.com/" style="color: #ededed;text-decoration: none;font-weight: normal;font-style: normal;">
													<img src="https://files.carrotquest.io/messenger/1453885630755-264b9f1e-b2c2-4e51-947e-247cb3f21a1e.png" editable="True" alt="facebook" width="32" height="32" border="0" style=" display: inline-block;">
												</a>
												&nbsp;
												<a href="http://vk.com/feed" style="color: #ededed;text-decoration: none;font-weight: normal;font-style: normal;">
													<img src="https://files.carrotquest.io/messenger/145388Lorem ipsum dolor sit amet, seddiam nonumy eirmod temporari invidunt ut labore et dolore magna aliquyam erat, sed diam. At vero eos et accusam et justo duo dolores.5577999-d7f2faee-862d-4b2b-8a09-a71899774e4a.png" editable="True" alt="vk" width="32" height="32" border="0" style=" display: inline-block;">
												</a>
												&nbsp;
												<a href="https://twitter.com/" style="color: #ededed;text-decoration: none;font-weight: normal;font-style: normal;">
													<img src="https://files.carrotquest.io/messenger/1453885642474-8347e284-4992-4c8b-bc19-58c124ecfcd3.png" editable="True" alt="twitter" width="32" height="32" border="0" style="display: inline-block;">
												</a>
												&nbsp;
												<a href="https://www.instagram.com/" style="color: #ededed;text-decoration: none;font-weight: normal;font-style: normal;">
													<img src="https://files.carrotquest.io/messenger/1453885637366-c1ebc17a-8ea3-4241-b584-aaf4af5e7ec0.png" editable="True" alt="instagram" width="32" height="32" border="0" style=" display: inline-block;">
												</a>
												&nbsp;
												</span>
											</td>
										</tr>
									</tbody></table>
									<!-- END OF RIGHT COLUMN-->
								</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
			</tbody></table>
		</td>
	</tr>
</tbody></table>
<!-- END OF SOCIAL BLOCK-->




</body></html>