<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contacts`.
 */
class m180118_122557_create_contacts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contacts', [
            'id' => $this->primaryKey(),
			'addressname' => $this->text()->notNull(),
			'client_name' => $this->string()->notNull(),
			'phone' => $this->string(11),
			'email' => $this->string(),
			'sigment1' => $this->text(),
			'sigment2' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('contacts');
    }
}
