<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notifications`.
 */
class m180118_121043_create_notifications_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('notifications', [
            'id' => $this->primaryKey(),
			'title' => $this->string()->notNull()->comment('Название рассылки'),
			'heading' => $this->string()->comment('Заголовок сообщения'),
			'content' => $this->text()->comment('Тело сообщения'),
			'email' => $this->boolean()->defaultValue(true),
			'sms' => $this->boolean()->defaultValue(false),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('notifications');
    }
}
