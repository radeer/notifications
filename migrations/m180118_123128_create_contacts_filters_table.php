<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contacts_filters`.
 */
class m180118_123128_create_contacts_filters_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contacts_filters', [
            'id' => $this->primaryKey(),
			'contact_id' => $this->integer()->notNull(),
			'filter_id' => $this->integer()->notNull(),
        ]);
		
		$this->createIndex('index_contact_id_filter_id', 'contacts_filters', ['contact_id', 'filter_id'], true);
		$this->addForeignKey('fk_contacts_filters_contacts', 'contacts_filters', 'contact_id', 'contacts', 'id', 'CASCADE', 'RESTRICT');
		$this->addForeignKey('fk_contacts_filters_filters', 'contacts_filters', 'filter_id', 'filters', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
		$this->dropForeignKey('fk_contacts_filters_contacts', 'contacts_filters');
		$this->dropForeignKey('fk_contacts_filters_filters', 'contacts_filters');
		$this->dropIndex('index_contact_id_filter_id', 'contacts_filters');
        $this->dropTable('contacts_filters');
    }
}
